package com.ra.shop.repository.implementation;

import com.ra.shop.model.Goods;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class GoodsRepositoryMockTest {

    @InjectMocks
    private GoodsRepositoryImpl dao;

    @Mock
    GoodsRepository goodsRepository;

    private static Goods TEST_GOODS = new Goods(1L, "Camel", 7622210609779L, 1.2d);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCreatedGoodsWithFalseNextIdThenReturnEqualsGoods() {
        Mockito.when(goodsRepository.save(TEST_GOODS)).thenReturn(TEST_GOODS);
        assertEquals(TEST_GOODS, dao.create(TEST_GOODS));
    }

    @Test
    void whenReadAllGoodThenReturnsList() {
        Mockito.when(goodsRepository.findAll()).thenReturn(new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        });
        assertEquals(3, dao.getAll().size());
    }

    @Test
    void whenDeleteGoodsThenReturnTrue() {
        assertTrue(dao.delete(TEST_GOODS.getId()));
    }

    @Test
    void whenGetGoodsWithTrueNextGoodsThenReturnEqualsGoods() {
        Mockito.when(goodsRepository.findById(Matchers.anyLong())).thenReturn(Optional.of(TEST_GOODS));
        assertEquals(TEST_GOODS, dao.get(TEST_GOODS.getId()).get());
    }

    @Test
    void whenUpdateGoodsThenReturnEqualsGoods() {
        Mockito.when(goodsRepository.save(TEST_GOODS)).thenReturn(TEST_GOODS);
        assertEquals(TEST_GOODS, dao.update(TEST_GOODS));
    }
}

