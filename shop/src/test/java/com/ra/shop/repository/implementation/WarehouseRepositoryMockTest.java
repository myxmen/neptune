package com.ra.shop.repository.implementation;

import com.ra.shop.model.Warehouse;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class WarehouseRepositoryMockTest {

    @InjectMocks
    private WarehouseRepositoryImpl warehouseDao;

    @Mock
    WarehouseRepository warehouseRepository;

    private static Warehouse TEST_WAREHOUSE = new Warehouse(1L, "Lola", Double.MIN_VALUE, Integer.MAX_VALUE);

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCreateMethodExecutedThenCorrectEntityReturns() {
        Mockito.when(warehouseRepository.save(TEST_WAREHOUSE)).thenReturn(TEST_WAREHOUSE);
        assertEquals(TEST_WAREHOUSE, warehouseDao.create(TEST_WAREHOUSE));
    }

    @Test
    void whenUpdateMethodExecutedThenCorrectEntityReturns() {
        Mockito.when(warehouseRepository.save(TEST_WAREHOUSE)).thenReturn(TEST_WAREHOUSE);
        assertEquals(TEST_WAREHOUSE, warehouseDao.update(TEST_WAREHOUSE));
    }

    @Test
    void whenDeleteCorrectlyExecutedThenReturnTrue() {
        assertTrue(warehouseDao.delete(TEST_WAREHOUSE.getId()));
    }

    @Test
    void whenGetByIdCorrectlyExecutedThenReturnWarehouse() {
        Mockito.when(warehouseRepository.findById(Matchers.anyLong())).thenReturn(Optional.of(TEST_WAREHOUSE));
           assertEquals(TEST_WAREHOUSE, warehouseDao.get(TEST_WAREHOUSE.getId()).get());
    }

    @Test
    void whenGetAllCorrectlyThenReturnList() {
        Mockito.when(warehouseRepository.findAll()).thenReturn(new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        });
        assertEquals(3, warehouseDao.getAll().size());
    }
}
