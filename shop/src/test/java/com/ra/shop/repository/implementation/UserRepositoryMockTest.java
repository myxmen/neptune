package com.ra.shop.repository.implementation;

import com.ra.shop.model.User;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class UserRepositoryMockTest {

    @InjectMocks
    private UserRepositoryImpl repository;

    @Mock
    UserRepository userRepository;

    private static User TEST_USER = new User(1L, "3806642341542", "Murchik", "Babulin",
            "USA", "murchik_21@gmail.com");

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCreateUserThenReturnCreatedUser() {
        Mockito.when(userRepository.save(TEST_USER)).thenReturn(TEST_USER);
        assertEquals(TEST_USER, repository.create(TEST_USER));
    }

    @Test
    void whenGetUserThenReturnCorrectEntity() {
        Mockito.when(userRepository.findById(Matchers.anyLong())).thenReturn(Optional.of(TEST_USER));
        assertEquals(TEST_USER, repository.get(TEST_USER.getId()).get());
    }

    @Test
    void whenDeleteUserThenReturnSuccessfulQueryExecutionResultTrue() {
        assertTrue(repository.delete(TEST_USER.getId()));
    }

    @Test
    void whenGetAllThenReturnListOfExistedUsers() {
        Mockito.when(userRepository.findAll()).thenReturn(new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        });
        assertEquals(3, repository.getAll().size());
    }

    @Test
    void whenUpdateUserThenReturnUpdatedUser() {
        Mockito.when(userRepository.save(TEST_USER)).thenReturn(TEST_USER);
        assertEquals(TEST_USER, repository.update(TEST_USER));
    }
}