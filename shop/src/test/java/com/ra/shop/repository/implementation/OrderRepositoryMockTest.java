package com.ra.shop.repository.implementation;

import com.ra.shop.model.Order;
import org.junit.jupiter.api.*;
import org.mockito.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class OrderRepositoryMockTest {

    @InjectMocks
    private OrderRepositoryImpl repository;

    @Mock
    OrderRepository orderRepository;

    private static Order TEST_ORDER = new Order(1L, 10, 100d, false, 0, false);

    @BeforeEach
    public void setup() throws SQLException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCreateOrderThenReturnCreatedOrder() {
        Mockito.when(orderRepository.save(TEST_ORDER)).thenReturn(TEST_ORDER);
        assertEquals(TEST_ORDER, repository.create(TEST_ORDER));
    }

    @Test
    void whenGetOrderThenReturnCorrectEntity() {
        Mockito.when(orderRepository.findById(Matchers.anyLong())).thenReturn(Optional.of(TEST_ORDER));
        assertEquals(TEST_ORDER, repository.get(TEST_ORDER.getId()).get());
    }

    @Test
    void whenDeleteOrderThenReturnSuccessfulQueryExecutionResultTrue() {
        assertTrue(repository.delete(TEST_ORDER.getId()));
    }

    @Test
    void whenGetAllThenReturnListOfExistedOrders() {
        Mockito.when(orderRepository.findAll()).thenReturn(new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        });
        assertEquals(3, repository.getAll().size());
    }

    @Test
    void whenUpdateOrderThenReturnUpdatedOrder() {
        Mockito.when(orderRepository.save(TEST_ORDER)).thenReturn(TEST_ORDER);
        assertEquals(TEST_ORDER, repository.update(TEST_ORDER));
    }
}