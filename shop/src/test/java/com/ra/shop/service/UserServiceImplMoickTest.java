package com.ra.shop.service;

import com.ra.shop.dto.UserDTO;
import com.ra.shop.model.User;
import com.ra.shop.repository.implementation.UserRepositoryImpl;
import com.ra.shop.service.impl.UserServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;

import javax.validation.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceImplMoickTest {

    private static UserServiceImpl userService;
    private UserDTO userDTO;
    private User user;

    @Mock
    private Validator validator;

    @Mock
    private UserRepositoryImpl userRepository;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(userRepository);
        user = new User(null, "3806642341542", "Murchik", "Babulin",
                "USA", "murchik_21@gmail.com");
        userDTO = new UserDTO(null, "3806642341542", "Murchik", "Babulin",
                "USA", "murchik_21@gmail.com");
    }

    @Test
    void whenCreateUserThanReturnCreatedUser() {
        validator.validate(userDTO);
        userService.create(userDTO);
        verify(validator, times(1)).validate(userDTO);
        verify(userRepository, atMost(1)).create(user);
    }

    @Test
    void whenGetAllThenReturnAllUsers() {
        when(userRepository.getAll()).thenReturn(Arrays.asList(user));
        assertFalse(userService.getAll().isEmpty());
    }

    @Test
    void whenValidationFailedInCreateMethodThenThrowValidationException() {
        userDTO.setPhoneNumber(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(userDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            userService.create(userDTO);
        });
        verify(userRepository, never()).create(user);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenGetUserThenReturnFoundUser() {
        when(userRepository.get(anyLong())).thenReturn(Optional.of(user));
        userDTO.setUserId(1L);
        userService.get(userDTO.getUserId());
        verify(userRepository, atMost(1)).get(user.getId());
    }

    @Test
    void whenUpdateUserDTOThanReturnUpdatedUser() {
        userDTO.setPhoneNumber("222");
        userDTO.setName("Bob");
        userService.update(userDTO);
        verify(userRepository, atMost(1)).update(user);
    }

    @Test
    void whenValidationFailedInUpdateMethodThenThrowValidationException() {
        userDTO.setPhoneNumber(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(userDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            userService.update(userDTO);
        });
        verify(userRepository, never()).update(user);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenDeleteUserThanReturnTrue() {
        userService.delete(userDTO.getUserId());
        verify(userRepository, atMost(1)).delete(user.getId());
    }
}
