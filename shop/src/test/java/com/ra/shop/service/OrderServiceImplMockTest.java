package com.ra.shop.service;

import com.ra.shop.dto.OrderDTO;
import com.ra.shop.model.Order;
import com.ra.shop.repository.implementation.OrderRepositoryImpl;
import com.ra.shop.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;

import javax.validation.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class OrderServiceImplMockTest {

    private static OrderServiceImpl orderService;
    private OrderDTO orderDTO;
    private Order order;

    @Mock
    private Validator validator;

    @Mock
    private OrderRepositoryImpl orderRepository;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        orderService = new OrderServiceImpl(orderRepository);
        order = new Order(null, 190, 210d, false, 0, true);
        orderDTO = new OrderDTO(null, 190, 210d, false, 0, true);
    }

    @Test
    void whenCreateOrderThanReturnCreatedOrder() {
        validator.validate(orderDTO);
        orderService.create(orderDTO);
        verify(validator, times(1)).validate(orderDTO);
        verify(orderRepository, atMost(1)).create(order);
    }

    @Test
    void whenGetAllThenReturnAllOrders() {
        when(orderRepository.getAll()).thenReturn(Arrays.asList(order));
        assertFalse(orderService.getAll().isEmpty());
    }

    @Test
    void whenValidationFailedInCreateMethodThenThrowValidationException() {
        orderDTO.setNumber(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(orderDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            orderService.create(orderDTO);
        });
        verify(orderRepository, never()).create(order);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenGetOrderThenReturnFoundOrder(){
        when(orderRepository.get(anyLong())).thenReturn(Optional.of(order));
        orderDTO.setOrderId(1L);
        orderService.get(orderDTO.getOrderId());
        verify(orderRepository, atMost(1)).get(order.getId());
    }

    @Test
    void whenUpdateOrderDTOThanReturnUpdatedOrder() {
        orderDTO.setNumber(222);
        orderDTO.setPrice(999d);
        orderService.update(orderDTO);
        verify(orderRepository, atMost(1)).update(order);
    }

    @Test
    void whenValidationFailedInUpdateMethodThenThrowValidationException(){
        orderDTO.setNumber(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(orderDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            orderService.update(orderDTO);
        });
        verify(orderRepository, never()).update(order);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenDeleteOrderThanReturnTrue() {
        orderService.delete(orderDTO.getOrderId());
        verify(orderRepository, atMost(1)).delete(order.getId());
    }
}
