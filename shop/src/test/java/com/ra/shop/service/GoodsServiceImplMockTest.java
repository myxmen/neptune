package com.ra.shop.service;

import com.ra.shop.dto.GoodsDTO;
import com.ra.shop.model.Goods;
import com.ra.shop.repository.implementation.GoodsRepositoryImpl;
import com.ra.shop.service.impl.GoodsServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;

import javax.validation.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GoodsServiceImplMockTest {

    private static GoodsServiceImpl goodsService;
    private GoodsDTO goodsDTO;
    private Goods goods;

    @Mock
    private Validator validator;

    @Mock
    private GoodsRepositoryImpl goodsRepository;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        goodsService = new GoodsServiceImpl(goodsRepository);
        goods = new Goods(null, "Camel", 7622210609779L, 1.2d);
        goodsDTO = new GoodsDTO(null, "Camel", 7622210609779L, 1.2d);
    }

    @Test
    void whenCreateGoodsThanReturnCreatedGoods() {
        validator.validate(goodsDTO);
        goodsService.create(goodsDTO);
        verify(validator, times(1)).validate(goodsDTO);
        verify(goodsRepository, atMost(1)).create(goods);
    }

    @Test
    void whenGetAllThenReturnAllGoods() {
        when(goodsRepository.getAll()).thenReturn(Arrays.asList(goods));
        assertFalse(goodsService.getAll().isEmpty());
    }

    @Test
    void whenValidationFailedInCreateMethodThenThrowValidationException() {
        goodsDTO.setName(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(goodsDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            goodsService.create(goodsDTO);
        });
        verify(goodsRepository, never()).create(goods);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenGetGoodsThenReturnFoundGoods() {
        when(goodsRepository.get(anyLong())).thenReturn(Optional.of(goods));
        goodsDTO.setGoodsId(1L);
        goodsService.get(goodsDTO.getGoodsId());
        verify(goodsRepository, atMost(1)).get(goods.getId());
    }

    @Test
    void whenUpdateGoodsDTOThanReturnUpdatedGoods() {
        goodsDTO.setName("LM");
        goodsDTO.setPrice(999d);
        goodsService.update(goodsDTO);
        verify(goodsRepository, atMost(1)).update(goods);
    }

    @Test
    void whenValidationFailedInUpdateMethodThenThrowValidationException() {
        goodsDTO.setName(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(goodsDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            goodsService.update(goodsDTO);
        });
        verify(goodsRepository, never()).update(goods);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenDeleteGoodsThanReturnTrue() {
        goodsService.delete(goodsDTO.getGoodsId());
        verify(goodsRepository, atMost(1)).delete(goods.getId());
    }
}
