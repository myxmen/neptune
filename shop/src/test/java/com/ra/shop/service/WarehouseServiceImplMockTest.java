package com.ra.shop.service;

import com.ra.shop.dto.WarehouseDTO;
import com.ra.shop.model.Warehouse;
import com.ra.shop.repository.implementation.WarehouseRepositoryImpl;
import com.ra.shop.service.impl.WarehouseServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;

import javax.validation.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class WarehouseServiceImplMockTest {

    private static WarehouseServiceImpl warehouseService;
    private WarehouseDTO warehouseDTO;
    private Warehouse warehouse;

    @Mock
    private Validator validator;

    @Mock
    private WarehouseRepositoryImpl warehouseRepository;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        warehouseService = new WarehouseServiceImpl(warehouseRepository);
        warehouse = new Warehouse(null, "Lola", Double.MIN_VALUE, Integer.MAX_VALUE);
        warehouseDTO = new WarehouseDTO(null, "Lola", Double.MIN_VALUE, Integer.MAX_VALUE);
    }

    @Test
    void whenCreateWarehouseThanReturnCreatedWarehouse() {
        validator.validate(warehouseDTO);
        warehouseService.create(warehouseDTO);
        verify(validator, times(1)).validate(warehouseDTO);
        verify(warehouseRepository, atMost(1)).create(warehouse);
    }

    @Test
    void whenGetAllThenReturnAllWarehouses() {
        when(warehouseRepository.getAll()).thenReturn(Arrays.asList(warehouse));
        assertFalse(warehouseService.getAll().isEmpty());
    }

    @Test
    void whenValidationFailedInCreateMethodThenThrowValidationException() {
        warehouseDTO.setName(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(warehouseDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            warehouseService.create(warehouseDTO);
        });
        verify(warehouseRepository, never()).create(warehouse);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenGetWarehouseThenReturnFoundWarehouse() {
        when(warehouseRepository.get(anyLong())).thenReturn(Optional.of(warehouse));
        warehouseDTO.setWarehouseId(1L);
        warehouseService.get(warehouseDTO.getWarehouseId());
        verify(warehouseRepository, atMost(1)).get(warehouse.getId());
    }

    @Test
    void whenUpdateWarehouseDTOThanReturnUpdatedWarehouse() {
        warehouseDTO.setName("Bob");
        warehouseDTO.setPrice(999d);
        warehouseService.update(warehouseDTO);
        verify(warehouseRepository, atMost(1)).update(warehouse);
    }

    @Test
    void whenValidationFailedInUpdateMethodThenThrowValidationException() {
        warehouseDTO.setName(null);
        doThrow(new ValidationException("may not be null")).when(validator).validate(warehouseDTO);
        Throwable validationException = assertThrows(ValidationException.class, () -> {
            warehouseService.update(warehouseDTO);
        });
        verify(warehouseRepository, never()).update(warehouse);
        assertEquals(ValidationException.class, validationException.getClass());
    }

    @Test
    void whenDeleteWarehouseThanReturnTrue() {
        warehouseService.delete(warehouseDTO.getWarehouseId());
        verify(warehouseRepository, atMost(1)).delete(warehouse.getId());
    }
}
