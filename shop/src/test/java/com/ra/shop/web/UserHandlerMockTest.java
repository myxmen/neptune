package com.ra.shop.web;

import com.ra.shop.dto.UserDTO;
import com.ra.shop.service.impl.UserServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

import java.util.Arrays;

public class UserHandlerMockTest {

    private UserController userController;
    private MockMvc mockMvc;
    private UserDTO userDTO;

    @Mock
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userDTO = new UserDTO(0l, "3806642341542", "Murchik", "Babulin",
                "USA", "murchik_21@gmail.com");
        userController = new UserController(userService);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        when(userService.getAll()).thenReturn(Arrays.asList(userDTO));
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/user")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("user", Arrays.asList(userDTO)))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showUser"));
        Mockito.verify(userService, Mockito.times(1)).getAll();
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/user")
                .flashAttr("user", userDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("user", userDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        Mockito.verify(userService, VerificationModeFactory.times(1)).create(userDTO);
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/user/{param.id}", id)
                .flashAttr("user", userDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        userDTO.setUserId(id);
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("user", userDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        Mockito.verify(userService, VerificationModeFactory.times(1)).update(userDTO);
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/user/{param.id}", id)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        Mockito.verify(userService, VerificationModeFactory.times(1)).delete(id);
    }
}
