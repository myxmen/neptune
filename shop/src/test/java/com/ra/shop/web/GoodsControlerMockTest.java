package com.ra.shop.web;

import com.ra.shop.dto.GoodsDTO;
import com.ra.shop.service.impl.GoodsServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import java.util.Arrays;

public class GoodsControlerMockTest {

    private GoodsController goodsController;
    private MockMvc mockMvc;
    private GoodsDTO goodsDTO;

    @Mock
    private GoodsServiceImpl goodsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        goodsDTO = new GoodsDTO(0l, "Camel", 7622210609779L, 1.2d);
        goodsController = new GoodsController(goodsService);
        mockMvc = MockMvcBuilders.standaloneSetup(goodsController).build();
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        when(goodsService.getAll()).thenReturn(Arrays.asList(goodsDTO));
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/goods")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("goods", Arrays.asList(goodsDTO)))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showGoods"));
        Mockito.verify(goodsService, Mockito.times(1)).getAll();
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/goods")
                .flashAttr("goods", goodsDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("goods", goodsDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        Mockito.verify(goodsService, times(1)).create(goodsDTO);
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/goods/{param.id}", id)
                .flashAttr("goods", goodsDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        goodsDTO.setGoodsId(id);
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("goods", goodsDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        Mockito.verify(goodsService, times(1)).update(goodsDTO);
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/goods/{param.id}", id)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        Mockito.verify(goodsService, times(1)).delete(id);
    }
}
