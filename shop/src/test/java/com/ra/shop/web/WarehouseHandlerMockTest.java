package com.ra.shop.web;

import com.ra.shop.dto.WarehouseDTO;
import com.ra.shop.service.impl.WarehouseServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

import java.util.Arrays;

public class WarehouseHandlerMockTest {

    private WarehouseController warehouseController;
    private MockMvc mockMvc;
    private WarehouseDTO warehouseDTO;

    @Mock
    private WarehouseServiceImpl warehouseService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        warehouseDTO = new WarehouseDTO(0l, "Lola", Double.MIN_VALUE, Integer.MAX_VALUE);
        warehouseController = new WarehouseController(warehouseService);
        mockMvc = MockMvcBuilders.standaloneSetup(warehouseController).build();
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        when(warehouseService.getAll()).thenReturn(Arrays.asList(warehouseDTO));
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/warehouse")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse", Arrays.asList(warehouseDTO)))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showWarehouse"));
        Mockito.verify(warehouseService, Mockito.times(1)).getAll();
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/warehouse")
                .flashAttr("warehouse", warehouseDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse", warehouseDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        Mockito.verify(warehouseService, VerificationModeFactory.times(1)).create(warehouseDTO);
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/warehouse/{param.id}", id)
                .flashAttr("warehouse", warehouseDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        warehouseDTO.setWarehouseId(id);
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse", warehouseDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        Mockito.verify(warehouseService, VerificationModeFactory.times(1)).update(warehouseDTO);
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/warehouse/{param.id}", id)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        Mockito.verify(warehouseService, VerificationModeFactory.times(1)).delete(id);
    }
}
