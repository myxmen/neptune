package com.ra.shop.web;

import com.ra.shop.dto.OrderDTO;
import com.ra.shop.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

import java.util.Arrays;

public class OrderHandlerMockTest {

    private OrderController orderController;
    private MockMvc mockMvc;
    private OrderDTO orderDTO;

    @Mock
    private OrderServiceImpl orderService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        orderDTO = new OrderDTO(0l, 10, 100d, false, 0, true);
        orderController = new OrderController(orderService);
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        when(orderService.getAll()).thenReturn(Arrays.asList(orderDTO));
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/order")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("order", Arrays.asList(orderDTO)))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showOrder"));
        Mockito.verify(orderService, Mockito.times(1)).getAll();
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/order")
                .flashAttr("order", orderDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("order", orderDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        Mockito.verify(orderService, VerificationModeFactory.times(1)).create(orderDTO);
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/order/{param.id}", id)
                .flashAttr("order", orderDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        orderDTO.setOrderId(id);
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("order", orderDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        Mockito.verify(orderService, VerificationModeFactory.times(1)).update(orderDTO);
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        var id = 1l;
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/order/{param.id}", id)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        Mockito.verify(orderService, VerificationModeFactory.times(1)).delete(id);
    }
}

