<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Users created</title>
</head>
<body>
<form action="/user" method="post" modelAttribute="user">
    <input required type="text" name="phoneNumber" path="phoneNumber" placeholder="Phone number">
    <input required type="text" name="name" path="name" placeholder="Name">
    <input required type="text" name="secondName" path="secondName" placeholder="Second name">
    <input required type="text" name="country" path="country" placeholder="Country">
    <input required type="text" name="emailAddress" path="emailAddress" placeholder="Email address">
    <input type="submit" value="Save">
</form>
</body>
</html>
