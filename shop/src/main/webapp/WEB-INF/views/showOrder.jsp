<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List order</title>
</head>
<body>

Table orders

<table border="2">
    <tr>
        <td>ID</td>
        <td>Number</td>
        <td>Price</td>
        <td>Delivery included</td>
        <td>Delivery cost</td>
        <td>Executed</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${order}" var="ordersone">
        <tr>
            <td>${ordersone.getOrderId()}</td>
            <td>${ordersone.getNumber()}</td>
            <td>${ordersone.getPrice()}</td>
            <td>${ordersone.getDeliveryIncluded()}</td>
            <td>${ordersone.getDeliveryCost()}</td>
            <td>${ordersone.getExecuted()}</td>
            <td>
                <form action="updateOrder" method="get">
                    <input type="hidden" name="id" value="${ordersone.getOrderId()}">
                    <input type="hidden" name="number" value="${ordersone.getNumber()}">
                    <input type="hidden" name="price" value="${ordersone.getPrice()}">
                    <input type="hidden" name="deliveryIncluded" value="${ordersone.getDeliveryIncluded()}">
                    <input type="hidden" name="deliveryCost" value="${ordersone.getDeliveryCost()}">
                    <input type="hidden" name="executed" value="${ordersone.getExecuted()}">
                    <input type="submit" value="Edit" style="float:left">
                </form>
                <form action="deleteOrder" method="get">
                    <input type="hidden" name="id" value="${ordersone.getOrderId()}">
                    <input type="submit" value="Delete" style="float:left">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="addOrder">
    <input type="submit" value="Add order">
</form>
<a href="/">Back</a>
</body>
</html>
