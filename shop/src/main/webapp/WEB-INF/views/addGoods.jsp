<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Goods created</title>
</head>
<body>

<form action="/goods" method="post" modelAttribute="goods">
    <input required type="text" name="name" path="name" placeholder="Name">
    <input required type="text" name="barcode" path="barcode" placeholder="Barcode">
    <input required type="text" name="price" path="price" placeholder="Price">
    <input type="submit" value="Save">
</form>
</body>
</html>

