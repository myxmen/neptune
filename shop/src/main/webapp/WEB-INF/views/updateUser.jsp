<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Edit user</title>
</head>
<body>

Update user

<form action="/user/${param.id}" method="post" modelAttribute="user">
    <input type="hidden" name="id" value="${param.id}">
    <input type="text" name="phoneNumber" path="phoneNumber"
           value="${param.phoneNumber}" placeholder=${param.phoneNumber}>
    <input type="text" name="name" path="name" value="${param.name}" placeholder=${param.name}>
    <input type="text" name="secondName" path="secondName" value="${param.secondName}" placeholder=${param.secondName}>
    <input type="text" name="country" path="country" value="${param.country}" placeholder=${param.country}>
    <input type="text" name="emailAddress" path="emailAddress"
           value="${param.emailAddress}" placeholder=${param.emailAddress}>
    <input type="hidden" name="_method" value="put">
    <input type="submit" value="Refresh">
</form>
<a href="user">Cancel</a>
</body>
</html>
