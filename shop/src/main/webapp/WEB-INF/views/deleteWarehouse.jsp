<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Delete warehouse</title>
</head>
<body>

Do you really want to delete the warehouse ${param.id}?

<form action="/warehouse/${param.id}" method="post">
    <input type="hidden" name="id" value="${param.id}">
    <input type="hidden" name="_method" value="delete">
    <a href="warehouse">Cancel</a>
    <input type="submit" value="Delete">
</form>
</body>
</html>
