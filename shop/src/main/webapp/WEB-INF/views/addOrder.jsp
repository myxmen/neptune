<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Order created</title>
</head>
<body>
<form action="/order" method="post" modelAttribute="order">
    <input required type="text" name="number" path="number" placeholder="Number">
    <input required type="text" name="price" path="price" placeholder="Price">
    <input required type="text" name="deliveryIncluded" path="deliveryIncluded" placeholder="Delivery included">
    <input required type="text" name="deliveryCost" path="deliveryCost" placeholder="Delivery cost">
    <input required type="text" name="executed" path="executed" placeholder="Executed">
    <input type="submit" value="Save">
</form>

</body>
</html>
