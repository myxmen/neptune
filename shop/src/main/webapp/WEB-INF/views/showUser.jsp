<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List user</title>
</head>
<body>

Table users

<table border="2">
    <tr>
        <td>ID</td>
        <td>Phone number</td>
        <td>Name</td>
        <td>Second name</td>
        <td>Country</td>
        <td>Email address</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${user}" var="usersone">
        <tr>
            <td>${usersone.getUserId()}</td>
            <td>${usersone.getPhoneNumber()}</td>
            <td>${usersone.getName()}</td>
            <td>${usersone.getSecondName()}</td>
            <td>${usersone.getCountry()}</td>
            <td>${usersone.getEmailAddress()}</td>
            <td>
                <form action="updateUser" method="get">
                    <input type="hidden" name="id" value="${usersone.getUserId()}">
                    <input type="hidden" name="phoneNumber" value="${usersone.getPhoneNumber()}">
                    <input type="hidden" name="name" value="${usersone.getName()}">
                    <input type="hidden" name="secondName" value="${usersone.getSecondName()}">
                    <input type="hidden" name="country" value="${usersone.getCountry()}">
                    <input type="hidden" name="emailAddress" value="${usersone.getEmailAddress()}">
                    <input type="submit" value="Edit" style="float:left">
                </form>
                <form action="deleteUser" method="get">
                    <input type="hidden" name="id" value="${usersone.getUserId()}">
                    <input type="submit" value="Delete" style="float:left">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="addUser">
    <input type="submit" value="Add user">
</form>
<a href="/">Back</a>
</body>
</html>
