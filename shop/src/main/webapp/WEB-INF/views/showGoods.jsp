<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List goods</title>
</head>
<body>

Table goods

<table border="2">
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Barcode</td>
        <td>Price</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${goods}" var="goodsone">
        <tr>
            <td>${goodsone.getGoodsId()}</td>
            <td>${goodsone.getName()}</td>
            <td>${goodsone.getBarcode()}</td>
            <td>${goodsone.getPrice()}</td>
            <td>
                <form action="updateGoods" method="get">
                    <input type="hidden" name="id" value="${goodsone.getGoodsId()}">
                    <input type="hidden" name="name" value="${goodsone.getName()}">
                    <input type="hidden" name="barcode" value="${goodsone.getBarcode()}">
                    <input type="hidden" name="price" value="${goodsone.getPrice()}">
                    <input type="submit" value="Edit" style="float:left">
                </form>
                <form action="deleteGoods" method="get">
                    <input type="hidden" name="id" value="${goodsone.getGoodsId()}">
                    <input type="submit" value="Delete" style="float:left">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="addGoods">
    <input type="submit" value="Add goods">
</form>

<a href="/">Back</a>
</body>
</html>