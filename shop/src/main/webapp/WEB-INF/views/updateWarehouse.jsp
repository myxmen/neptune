<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Edit warehouse</title>
</head>
<body>

Update warehouse

<form action="/warehouse/${param.id}" method="post" modelAttribute="warehouse">
    <input type="hidden" name="id" value="${param.id}">
    <input type="text" name="name" path="name" value="${param.name}" placeholder=${param.name}>
    <input type="text" name="price" path="price" value="${param.price}" placeholder=${param.price}>
    <input type="text" name="amount" path="amount" value="${param.amount}" placeholder=${param.amount}>
    <input type="hidden" name="_method" value="put">
    <input type="submit" value="Refresh">
</form>
<a href="warehouse">Cancel</a>

</body>
</html>
