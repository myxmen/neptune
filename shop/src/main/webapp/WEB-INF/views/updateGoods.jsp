<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Edit goods</title>
</head>
<body>

Update goods

<form action="/goods/${param.id}" method="post" modelAttribute="goods">
    <input type="hidden" name="id" value="${param.id}">
    <input type="text" name="name" path="name" value="${param.name}" placeholder=${param.name}>
    <input type="text" name="barcode" path="barcode" value="${param.barcode}" placeholder=${param.barcode}>
    <input type="text" name="price" path="price" value="${param.price}" placeholder=${param.price}>
    <input type="hidden" name="_method" value="put">
    <input type="submit" value="Refresh">
</form>
<a href="goods">Cancel</a>
</body>
</html>