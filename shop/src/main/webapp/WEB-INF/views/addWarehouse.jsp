<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Warehouse created</title>
</head>
<body>

<form action="/warehouse" method="post" modelAttribute="warehouse">
    <input required type="text" name="name" path="name" placeholder="Name">
    <input required type="text" name="price" path="price" placeholder="Price">
    <input required type="text" name="amount" path="amount" placeholder="Amount">
    <input type="submit" value="Save">
</form>
</body>
</html>
