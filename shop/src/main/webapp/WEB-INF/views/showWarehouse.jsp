<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List warehouse</title>
</head>
<body>

Table warehouse

<table border="2">
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Price</td>
        <td>Amount</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${warehouse}" var="warehouseone">
        <tr>
            <td>${warehouseone.getWarehouseId()}</td>
            <td>${warehouseone.getName()}</td>
            <td>${warehouseone.getPrice()}</td>
            <td>${warehouseone.getAmount()}</td>
            <td>
                <form action="updateWarehouse" method="get">
                    <input type="hidden" name="id" value="${warehouseone.getWarehouseId()}">
                    <input type="hidden" name="name" value="${warehouseone.getName()}">
                    <input type="hidden" name="price" value="${warehouseone.getPrice()}">
                    <input type="hidden" name="amount" value="${warehouseone.getAmount()}">
                    <input type="submit" value="Edit" style="float:left">
                </form>
                <form action="deleteWarehouse" method="get">
                    <input type="hidden" name="id" value="${warehouseone.getWarehouseId()}">
                    <input type="submit" value="Delete" style="float:left">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="addWarehouse">
    <input type="submit" value="Add warehouse">
</form>
<a href="/">Back</a>
</body>
</html>
