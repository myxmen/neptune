<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Edit order</title>
</head>
<body>

Update order

<form action="/order/${param.id}" method="post" modelAttribute="order">
    <input type="hidden" name="id" value="${param.id}">
    <input type="text" name="number" path="number" value="${param.number}" placeholder=${param.number}>
    <input type="text" name="price" path="price" value="${param.price}" placeholder=${param.price}>
    <input type="text" name="deliveryIncluded" path="deliveryIncluded"
           value="${param.deliveryIncluded}" placeholder=${param.deliveryIncluded}>
    <input type="text" name="deliveryCost" path="deliveryCost"
           value="${param.deliveryCost}" placeholder=${param.deliveryCost}>
    <input type="text" name="executed" path="executed" value="${param.executed}" placeholder=${param.executed}>
    <input type="hidden" name="_method" value="put">
    <input type="submit" value="Refresh">
</form>
<a href="order">Cancel</a>
</body>
</html>
