package com.ra.shop.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GoodsDTO {

    private Long goodsId;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Size(min = LengthHelper.MIN_NAME_GOODS, max = LengthHelper.MAX_NAME_GOODS, message = "Name is too short")
    private String name;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Barcode should be > 0")
    private Long barcode;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Price should be > 0")
    private Double price;
}
