package com.ra.shop.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {

    private Long userId;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    private String phoneNumber;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Size(min = LengthHelper.MIN_NAME_USER, max = LengthHelper.MAX_NAME_USER, message = "Name is too short")
    private String name;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Size(min = LengthHelper.MIN_SNAME_USER, max = LengthHelper.MAX_SNAME_USER, message = "Secondname is too short")
    private String secondName;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Size(min = LengthHelper.MIN_CONTRY_USER, max = LengthHelper.MAX_CONTRY_USER, message = "Country is too short")
    private String country;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Email
    private String emailAddress;
}
