package com.ra.shop.dto;

public class LengthHelper {

    /**
     * Message texts for fields validation of GoodsDTO.
     */
    public static final String MES_NOT_NULL = "may not be null";

    /**
     * Min/Max Constants for fields validation of GoodsDTO.
     */
    public static final int MIN_NAME_GOODS = 2;
    public static final int MAX_NAME_GOODS = 100;

    /**
     * Min/Max Constants for fields validation of UserDTO.
     */
    public static final int MIN_NAME_USER = 3;
    public static final int MAX_NAME_USER = 100;
    public static final int MIN_SNAME_USER = 4;
    public static final int MAX_SNAME_USER = 100;
    public static final int MIN_CONTRY_USER = 2;
    public static final int MAX_CONTRY_USER = 100;

    /**
     * Min/Max Constants for fields validation of WarehouseDTO.
     */
    public static final int MIN_NAME_WH = 2;
    public static final int MAX_NAME_WH = 100;
}