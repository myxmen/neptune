package com.ra.shop.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderDTO {

    private Long orderId;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Order number must be at least 1")
    private Integer number;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Price should be > 0")
    private Double price;

    private Boolean deliveryIncluded;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @PositiveOrZero(message = "Delivery cost should be >= 0")
    private Integer deliveryCost;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    private Boolean executed;

}
