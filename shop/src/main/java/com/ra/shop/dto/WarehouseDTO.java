package com.ra.shop.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WarehouseDTO {

    private Long warehouseId;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Size(min = LengthHelper.MIN_NAME_WH, max = LengthHelper.MAX_NAME_WH, message = "Name is too short")
    private String name;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Price should be > 0")
    private Double price;

    @NotNull(message = LengthHelper.MES_NOT_NULL)
    @Positive(message = "Amount should be > 0")
    private Integer amount;

}
