package com.ra.shop.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import com.ra.shop.dto.OrderDTO;
import com.ra.shop.model.Order;
import com.ra.shop.repository.IRepository;
import com.ra.shop.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of IService interface.
 */
@Service
public class OrderServiceImpl implements IService<OrderDTO, Order> {

    private final transient IRepository<Order> orderIRepository;
    private final transient Validator validator;

    @Autowired
    public OrderServiceImpl(final IRepository<Order> orderIRepository) {
        this.orderIRepository = orderIRepository;
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public Order create(final OrderDTO dtoEntity) {
        final Set<ConstraintViolation<OrderDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return orderIRepository.create(createOrderFromDTO(dtoEntity));
    }

    @Override
    public Order get(final Long entityId) {
        return orderIRepository.get(entityId).orElse(new Order());
    }

    @Override
    public Order update(final OrderDTO dtoEntity) {
        final Set<ConstraintViolation<OrderDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return orderIRepository.update(createOrderFromDTO(dtoEntity));
    }

    @Override
    public boolean delete(final Long entityId) {
        return orderIRepository.delete(entityId);
    }

    @Override
    public List<OrderDTO> getAll() {
        return orderIRepository.getAll().stream()
                .map(entity -> new OrderDTO(entity.getId(), entity.getNumber(), entity.getPrice(), entity.getDeliveryIncluded(),
                        entity.getDeliveryCost(), entity.getExecuted()))
                .collect(Collectors.toList());
    }

    /**
     * Method set all parameters from order dto to order.
     *
     * @param orderDTO order DTO.
     * @return Order object.
     */
    private Order createOrderFromDTO(final OrderDTO orderDTO) {
        return new Order(
                orderDTO.getOrderId(),
                orderDTO.getNumber(),
                orderDTO.getPrice(),
                orderDTO.getDeliveryIncluded(),
                orderDTO.getDeliveryCost(),
                orderDTO.getExecuted());
    }
}
