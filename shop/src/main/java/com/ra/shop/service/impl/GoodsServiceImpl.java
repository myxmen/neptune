package com.ra.shop.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import com.ra.shop.dto.GoodsDTO;
import com.ra.shop.model.Goods;
import com.ra.shop.repository.IRepository;
import com.ra.shop.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of IService interface.
 */
@Service
public class GoodsServiceImpl implements IService<GoodsDTO, Goods> {

    private final transient IRepository<Goods> goodsIRepository;
    private final transient Validator validator;

    @Autowired
    public GoodsServiceImpl(final IRepository<Goods> goodsIRepository) {
        this.goodsIRepository = goodsIRepository;
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public Goods create(final GoodsDTO dtoEntity) {
        final Set<ConstraintViolation<GoodsDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return goodsIRepository.create(createGoodsFromDTO(dtoEntity));
    }

    @Override
    public Goods get(final Long dtoEntityId) {
        return goodsIRepository.get(dtoEntityId).orElse(new Goods());
    }

    @Override
    public Goods update(final GoodsDTO dtoEntity) {
        final Set<ConstraintViolation<GoodsDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return goodsIRepository.update(createGoodsFromDTO(dtoEntity));
    }

    @Override
    public boolean delete(final Long entityId) {
        return goodsIRepository.delete(entityId);
    }

    @Override
    public List<GoodsDTO> getAll() {
        return goodsIRepository.getAll().stream()
                .map(entity -> new GoodsDTO(entity.getId(), entity.getName(), entity.getBarcode(), entity.getPrice()))
                .collect(Collectors.toList());
    }

    /**
     * Method set all parameters from goods dto to goods.
     *
     * @param goodsDTO goods DTO.
     * @return Goods object.
     */
    private Goods createGoodsFromDTO(final GoodsDTO goodsDTO) {
        return new Goods(
                goodsDTO.getGoodsId(),
                goodsDTO.getName(),
                goodsDTO.getBarcode(),
                goodsDTO.getPrice());
    }
}
