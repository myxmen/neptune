package com.ra.shop.service;

import java.util.List;

/**
 * Interface represents simple CRUD-methods.
 *
 * @param <D> DTO type.
 * @param <T> type that will be stored in database.
 */
public interface IService<D, T> {

    /**
     * Method inserts new entity to database.
     *
     * @param dtoEntity dto object.
     * @return T Entity that will be stored in database.
     */
    T create(D dtoEntity);

    /**
     * Method returns an entity from database.
     *
     * @param dtoEntityId dto entity Id.
     * @return T object from database.
     */
    T get(Long dtoEntityId);

    /**
     * Method updates existed entity due to it`s new params and send updated entity to database.
     *
     * @param dtoEntity dto entity.
     * @return T updated entity.
     */
    T update(D dtoEntity);

    /**
     * Method will delete entity from the database.
     *
     * @param entityId dto entity Id.
     * @return true if entity successfully deleted, false if not.
     */
    boolean delete(Long entityId);

    /**
     * Method returns all entities.
     *
     * @return List of T returns all entities from database.
     */
    List<D> getAll();

}
