package com.ra.shop.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import com.ra.shop.dto.UserDTO;
import com.ra.shop.model.User;
import com.ra.shop.repository.IRepository;
import com.ra.shop.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of IService interface.
 */
@Service
public class UserServiceImpl implements IService<UserDTO, User> {

    private final transient IRepository<User> userIRepository;
    private final transient Validator validator;

    @Autowired
    public UserServiceImpl(final IRepository<User> userIRepository) {
        this.userIRepository = userIRepository;
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public User create(final UserDTO dtoEntity) {
        final Set<ConstraintViolation<UserDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return userIRepository.create(createUserFromDTO(dtoEntity));
    }

    @Override
    public User get(final Long dtoEntityId) {
        return userIRepository.get(dtoEntityId).orElse(new User());
    }

    @Override
    public User update(final UserDTO dtoEntity) {
        final Set<ConstraintViolation<UserDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return userIRepository.update(createUserFromDTO(dtoEntity));
    }

    @Override
    public boolean delete(final Long entityId) {
        return userIRepository.delete(entityId);
    }

    @Override
    public List<UserDTO> getAll() {
        return userIRepository.getAll().stream()
                .map(entity -> new UserDTO(entity.getId(), entity.getPhoneNumber(), entity.getName(), entity.getSecondName(),
                        entity.getCountry(), entity.getEmailAddress()))
                .collect(Collectors.toList());
    }

    /**
     * Method set all parameters from user dto to user.
     *
     * @param userDTO user DTO.
     * @return User object.
     */
    private User createUserFromDTO(final UserDTO userDTO) {
        return new User(
                userDTO.getUserId(),
                userDTO.getPhoneNumber(),
                userDTO.getName(),
                userDTO.getSecondName(),
                userDTO.getCountry(),
                userDTO.getEmailAddress());
    }
}
