package com.ra.shop.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import com.ra.shop.dto.WarehouseDTO;
import com.ra.shop.model.Warehouse;
import com.ra.shop.repository.IRepository;
import com.ra.shop.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of IService interface.
 */
@Service
public class WarehouseServiceImpl implements IService<WarehouseDTO, Warehouse> {

    private final transient IRepository<Warehouse> whIRepository;
    private final transient Validator validator;

    @Autowired
    public WarehouseServiceImpl(final IRepository<Warehouse> whIRepository) {
        this.whIRepository = whIRepository;
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public Warehouse create(final WarehouseDTO dtoEntity) {
        final Set<ConstraintViolation<WarehouseDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return whIRepository.create(createWarehouseFromDTO(dtoEntity));
    }

    @Override
    public Warehouse get(final Long dtoEntityId) {
        return whIRepository.get(dtoEntityId).orElse(new Warehouse());
    }

    @Override
    public Warehouse update(final WarehouseDTO dtoEntity) {
        final Set<ConstraintViolation<WarehouseDTO>> violations = validator.validate(dtoEntity);
        if (!violations.isEmpty()) {
            final String message = violations.iterator().next().getMessage();
            throw new ValidationException(message);
        }
        return whIRepository.update(createWarehouseFromDTO(dtoEntity));
    }

    @Override
    public boolean delete(final Long entityId) {
        return whIRepository.delete(entityId);
    }

    @Override
    public List<WarehouseDTO> getAll() {
        return whIRepository.getAll().stream()
                .map(entity -> new WarehouseDTO(entity.getId(), entity.getName(), entity.getPrice(), entity.getAmount()))
                .collect(Collectors.toList());
    }

    /**
     * Method set all parameters from warehouse dto to warehouse.
     *
     * @param warehouseDTO warehouse DTO.
     * @return Warehouse object.
     */
    private Warehouse createWarehouseFromDTO(final WarehouseDTO warehouseDTO) {
        return new Warehouse(
                warehouseDTO.getWarehouseId(),
                warehouseDTO.getName(),
                warehouseDTO.getPrice(),
                warehouseDTO.getAmount());
    }
}
