package com.ra.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    /**
     * bean for InternalResourceViewResolver.
     *
     * @return viewResolver
     */
    @Bean
    public InternalResourceViewResolver internalResourceViewResolver() {
        final InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    /**
     * Add urlPath for Jsp.
     *
     * @param registry for ViewControllerRegistry.
     */
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");

        registry.addViewController("/addGoods").setViewName("addGoods");
        registry.addViewController("/updateGoods").setViewName("updateGoods");
        registry.addViewController("/deleteGoods").setViewName("deleteGoods");

        registry.addViewController("/addOrder").setViewName("addOrder");
        registry.addViewController("/updateOrder").setViewName("updateOrder");
        registry.addViewController("/deleteOrder").setViewName("deleteOrder");

        registry.addViewController("/addUser").setViewName("addUser");
        registry.addViewController("/updateUser").setViewName("updateUser");
        registry.addViewController("/deleteUser").setViewName("deleteUser");

        registry.addViewController("/addWarehouse").setViewName("addWarehouse");
        registry.addViewController("/updateWarehouse").setViewName("updateWarehouse");
        registry.addViewController("/deleteWarehouse").setViewName("deleteWarehouse");
    }
}
