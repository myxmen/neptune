package com.ra.shop.config;

import java.util.Properties;
import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.PostgreSQL82Dialect;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * Primary configuration class.
 */
@Configuration
@ComponentScan("com.ra.shop")
@PropertySource("classpath:cfg.properties")
@EnableJpaRepositories(basePackages = "com.ra.shop.repository",
        entityManagerFactoryRef = "entityManagerFactoryRef1", transactionManagerRef = "transactionManagerRef1")
public class ShopConfiguration {

    /**
     * Spring environment that stores database properties.
     */
    @Autowired
    private transient Environment environment;

    /**
     * Hikari DataSource with database properties.
     *
     * @return DataSource instance with configured database properties.
     */
    @Bean
    public DataSource buildDataSource() {
        final HikariConfig config = new HikariConfig();
        config.setDriverClassName(environment.getProperty("DRIVER_CLASS"));
        config.setJdbcUrl(environment.getProperty("URL"));
        config.setUsername(environment.getProperty("USERNAME"));
        config.setPassword(environment.getProperty("PASSWORD"));
        config.setMaximumPoolSize(Integer.valueOf(environment.getProperty("MAXIMUM_POOL_SIZE")));
        config.setIdleTimeout(Integer.valueOf(environment.getProperty("IDLE_TIMEOUT")));
        config.setMinimumIdle(Integer.valueOf(environment.getProperty("MINIMUM_IDLE")));
        config.setConnectionTimeout(Integer.valueOf(environment.getProperty("CONNECTION_TIMEOUT")));
        return new HikariDataSource(config);
    }

    @Bean(name = "transactionManagerRef1")
    public JpaTransactionManager jpaTransactionManager() {
        final JpaTransactionManager transManager = new JpaTransactionManager();
        transManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
        return transManager;
    }

    private HibernateJpaVendorAdapter vendorAdaptor() {
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(true);
        return vendorAdapter;
    }

    @Bean(name = "entityManagerFactoryRef1")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        final LocalContainerEntityManagerFactoryBean entityMFB = new LocalContainerEntityManagerFactoryBean();
        entityMFB.setJpaVendorAdapter(vendorAdaptor());
        entityMFB.setDataSource(buildDataSource());
        entityMFB.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityMFB.setPackagesToScan("com.ra.shop.model");
        entityMFB.setJpaProperties(hibernateProperties());
        return entityMFB;
    }

    private Properties hibernateProperties() {
        final Properties properties = new Properties();
        properties.setProperty(AvailableSettings.DIALECT, PostgreSQL82Dialect.class.getName());
        properties.setProperty(AvailableSettings.SHOW_SQL, String.valueOf(true));
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO, "update");
        return properties;
    }
}
