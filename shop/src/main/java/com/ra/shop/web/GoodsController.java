package com.ra.shop.web;

import com.ra.shop.dto.GoodsDTO;
import com.ra.shop.service.impl.GoodsServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Log4j2
@Controller
@RequestMapping("/goods")
public class GoodsController {

    /**
     * GoodsServiceImpl instance.
     */
    private final transient GoodsServiceImpl goodsService;

    /**
     * Public constructor. Accepts GoodsRepositoryImpl as a parameter.
     *
     * @param goodsService Goods goodsService.
     */
    @Autowired
    public GoodsController(final GoodsServiceImpl goodsService) {
        this.goodsService = goodsService;
    }

    /**
     * Performs POST method.
     *
     * @param goodsDto model for mapping
     * @return redirect Url
     */
    @PostMapping
    public String post(@ModelAttribute("goods") final GoodsDTO goodsDto) {
        goodsService.create(goodsDto);
        return "redirect:/goods";
    }

    /**
     * Performs GET method.
     *
     * @param model model for mapping
     * @return Url
     */
    @GetMapping
    public String get(final Model model) {
        model.addAttribute("goods", goodsService.getAll());
        return "showGoods";
    }

    /**
     * Performs PUT method.
     *
     * @param idForUpdate represents GoodsId
     * @param goodsDto    model for mapping
     * @return redirect Url
     */
    @PutMapping("/{param.id}")
    public String put(@PathVariable("param.id") final long idForUpdate,
                      @ModelAttribute("goods") final GoodsDTO goodsDto) {
        goodsDto.setGoodsId(idForUpdate);
        goodsService.update(goodsDto);
        return "redirect:/goods";
    }

    /**
     * Performs DELETE method.
     *
     * @param idForDelete represents GoodsId
     * @return redirect Url
     */
    @DeleteMapping("/{param.id}")
    public String delete(@PathVariable("param.id") final long idForDelete) {
        goodsService.delete(idForDelete);
        return "redirect:/goods";
    }
}
