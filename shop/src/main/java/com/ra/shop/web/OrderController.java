package com.ra.shop.web;

import com.ra.shop.dto.OrderDTO;
import com.ra.shop.service.impl.OrderServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Log4j2
@Controller
@RequestMapping("/order")
public class OrderController {

    /**
     * OrderServiceImpl instance.
     */
    private final transient OrderServiceImpl orderService;

    /**
     * Public constructor. Accepts OrderRepositoryImpl as a parameter.
     *
     * @param orderService Order orderService.
     */
    @Autowired
    public OrderController(final OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    /**
     * Performs POST method.
     *
     * @param orderDTO model for mapping
     * @return redirect Url
     */
    @PostMapping
    public String post(@ModelAttribute("order") final OrderDTO orderDTO) {
        orderService.create(orderDTO);
        return "redirect:/order";
    }

    /**
     * Performs GET method.
     *
     * @param model model for mapping
     * @return Url
     */
    @GetMapping
    public String get(final Model model) {
        model.addAttribute("order", orderService.getAll());
        return "showOrder";
    }

    /**
     * Performs PUT method.
     *
     * @param idForUpdate represents OrderId
     * @param orderDto    model for mapping
     * @return redirect Url
     */
    @PutMapping("/{param.id}")
    public String put(@PathVariable("param.id") final long idForUpdate,
                      @ModelAttribute("order") final OrderDTO orderDto) {
        orderDto.setOrderId(idForUpdate);
        orderService.update(orderDto);
        return "redirect:/order";
    }

    /**
     * Performs DELETE method.
     *
     * @param idForDelete represents OrderId
     * @return redirect Url
     */
    @DeleteMapping("/{param.id}")
    public String delete(@PathVariable("param.id") final long idForDelete) {
        orderService.delete(idForDelete);
        return "redirect:/order";
    }
}
