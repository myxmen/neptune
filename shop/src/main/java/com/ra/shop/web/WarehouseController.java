package com.ra.shop.web;

import com.ra.shop.dto.WarehouseDTO;
import com.ra.shop.service.impl.WarehouseServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Log4j2
@Controller
@RequestMapping("/warehouse")
public class WarehouseController {

    /**
     * WarehouseServiceImpl instance.
     */
    private final transient WarehouseServiceImpl warehouseService;

    /**
     * Public constructor. Accepts WarehouseRepositoryImpl as a parameter.
     *
     * @param warehouseService Warehouse warehouseService.
     */
    @Autowired
    public WarehouseController(final WarehouseServiceImpl warehouseService) {
        this.warehouseService = warehouseService;
    }

    /**
     * Performs POST method.
     *
     * @param warehouseDto model for mapping
     * @return redirect Url
     */
    @PostMapping
    public String post(@ModelAttribute("warehouse") final WarehouseDTO warehouseDto) {
        warehouseService.create(warehouseDto);
        return "redirect:/warehouse";
    }

    /**
     * Performs GET method.
     *
     * @param model model for mapping
     * @return Url
     */
    @GetMapping
    public String get(final Model model) {
        model.addAttribute("warehouse", warehouseService.getAll());
        return "showWarehouse";
    }

    /**
     * Performs PUT method.
     *
     * @param idForUpdate  represents WarehouseId
     * @param warehouseDto model for mapping
     * @return redirect Url
     */
    @PutMapping("/{param.id}")
    public String put(@PathVariable("param.id") final long idForUpdate,
                      @ModelAttribute("warehouse") final WarehouseDTO warehouseDto) {
        warehouseDto.setWarehouseId(idForUpdate);
        warehouseService.update(warehouseDto);
        return "redirect:/warehouse";
    }

    /**
     * Performs DELETE method.
     *
     * @param idForDelete represents WarehouseId
     * @return redirect Url
     */
    @DeleteMapping("/{param.id}")
    public String delete(@PathVariable("param.id") final long idForDelete) {
        warehouseService.delete(idForDelete);
        return "redirect:/warehouse";
    }
}
