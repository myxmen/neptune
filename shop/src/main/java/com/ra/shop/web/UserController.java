package com.ra.shop.web;

import com.ra.shop.dto.UserDTO;
import com.ra.shop.service.impl.UserServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Log4j2
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * UserServiceImpl instance.
     */
    private final transient UserServiceImpl userService;

    /**
     * Public constructor. Accepts UserRepositoryImpl as a parameter.
     *
     * @param userService User userService.
     */
    @Autowired
    public UserController(final UserServiceImpl userService) {
        this.userService = userService;
    }

    /**
     * Performs POST method.
     *
     * @param userDto model for mapping
     * @return redirect Url
     */
    @PostMapping
    public String post(@ModelAttribute("user") final UserDTO userDto) {
        userService.create(userDto);
        return "redirect:/user";
    }

    /**
     * Performs GET method.
     *
     * @param model model for mapping
     * @return Url
     */
    @GetMapping
    public String get(final Model model) {
        model.addAttribute("user", userService.getAll());
        return "showUser";
    }

    /**
     * Performs PUT method.
     *
     * @param idForUpdate represents UserId
     * @param userDto     model for mapping
     * @return redirect Url
     */
    @PutMapping("/{param.id}")
    public String put(@PathVariable("param.id") final long idForUpdate,
                      @ModelAttribute("user") final UserDTO userDto) {
        userDto.setUserId(idForUpdate);
        userService.update(userDto);
        return "redirect:/user";
    }

    /**
     * Performs DELETE method.
     *
     * @param idForDelete represents UserId
     * @return redirect Url
     */
    @DeleteMapping("/{param.id}")
    public String delete(@PathVariable("param.id") final long idForDelete) {
        userService.delete(idForDelete);
        return "redirect:/user";
    }
}
