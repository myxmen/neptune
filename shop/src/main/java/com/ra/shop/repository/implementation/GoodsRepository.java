package com.ra.shop.repository.implementation;

import com.ra.shop.model.Goods;
import com.ra.shop.repository.ShopEntityRepository;

public interface GoodsRepository extends ShopEntityRepository<Goods> {
}
