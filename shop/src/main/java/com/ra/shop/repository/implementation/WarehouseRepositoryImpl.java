package com.ra.shop.repository.implementation;

import java.util.List;
import java.util.Optional;

import com.ra.shop.model.Warehouse;
import com.ra.shop.repository.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WarehouseRepositoryImpl implements IRepository<Warehouse> {

    @Autowired
    private transient WarehouseRepository warehouseRep;

    /**
     * Method adds new Warehouse to the Data Base.
     *
     * @param warehouse to save
     * @return new Warehouse
     */
    @Override
    @Transactional
    public Warehouse create(final Warehouse warehouse) {
        return warehouseRep.save(warehouse);
    }

    /**
     * Update Warehouse to Data Base.
     *
     * @param warehouse to update
     * @return new Warehouse
     */
    @Override
    @Transactional
    public Warehouse update(final Warehouse warehouse) {
        return warehouseRep.save(warehouse);
    }

    /**
     * Method deletes the warehouse from a Data Base by Id.
     *
     * @param entityId Long we want delete
     * @return count of deleted rows
     */
    @Override
    @Transactional
    public boolean delete(final Long entityId) {
        warehouseRep.deleteById(entityId);
        return true;
    }

    /**
     * Method returns warehouse from a Data Base by Id.
     *
     * @param entityId Warehouse goodsId
     * @return Optional of warehouse or empty optional
     */
    @Override
    @Transactional
    public Optional<Warehouse> get(final Long entityId) {
        return warehouseRep.findById(entityId);
    }

    /**
     * Method returns List of Warehouses from a Data Base by Id.
     *
     * @return list of warehouses or empty list
     */
    @Override
    @Transactional
    public List<Warehouse> getAll() {
        return (List<Warehouse>) warehouseRep.findAll();
    }
}
