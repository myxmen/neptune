package com.ra.shop.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ShopEntityRepository<T> extends CrudRepository<T, Long> {
}
