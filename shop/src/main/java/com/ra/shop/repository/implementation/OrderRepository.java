package com.ra.shop.repository.implementation;

import com.ra.shop.model.Order;
import com.ra.shop.repository.ShopEntityRepository;

public interface OrderRepository extends ShopEntityRepository<Order> {
}
