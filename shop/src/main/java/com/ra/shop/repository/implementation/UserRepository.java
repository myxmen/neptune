package com.ra.shop.repository.implementation;

import com.ra.shop.model.User;
import com.ra.shop.repository.ShopEntityRepository;

public interface UserRepository extends ShopEntityRepository<User> {
}
