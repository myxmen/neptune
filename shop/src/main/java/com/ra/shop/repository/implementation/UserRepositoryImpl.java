package com.ra.shop.repository.implementation;

import java.util.List;
import java.util.Optional;

import com.ra.shop.model.User;
import com.ra.shop.repository.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * IRepository implementation using Spring jdbc.
 */
@Component
public class UserRepositoryImpl implements IRepository<User> {

    @Autowired
    private transient UserRepository userRepository;

    /**
     * Method adds an entity to database.
     *
     * @param entity that will be created.
     * @return User persisted entity.
     */
    @Override
    @Transactional
    public User create(final User entity) {
        return userRepository.save(entity);
    }

    /**
     * Method performs search in database and returns an entity with pointed Id.
     *
     * @param entityId - goodsId of searched entity.
     * @return User searched entity.
     */
    @Override
    @Transactional
    public Optional<User> get(final Long entityId) {
        return userRepository.findById(entityId);
    }

    /**
     * Method performs update operation.
     *
     * @param newEntity updated version of entity.
     * @return User updated version of entity.
     */
    @Override
    @Transactional
    public User update(final User newEntity) {
        return userRepository.save(newEntity);
    }

    /**
     * Method performs delete operation.
     *
     * @param entityId of entity that will be deleted.
     * @return boolean true if entity removed successfully or false if not.
     */
    @Override
    @Transactional
    public boolean delete(final Long entityId) {
        userRepository.deleteById(entityId);
        return true;
    }

    /**
     * Method returns list of entities that stored in database.
     *
     * @return List list of existed users.
     */
    @Override
    @Transactional
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }
}
