package com.ra.shop.repository.implementation;

import java.util.List;
import java.util.Optional;

import com.ra.shop.model.Order;
import com.ra.shop.repository.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * IRepository implementation using Spring jdbc.
 */
@Component
public class OrderRepositoryImpl implements IRepository<Order> {

    @Autowired
    private transient OrderRepository orderRepository;

    /**
     * Method adds an entity to database.
     *
     * @param entity that will be created.
     * @return Order persisted entity.
     */
    @Override
    @Transactional
    public Order create(final Order entity) {
        return orderRepository.save(entity);
    }

    /**
     * Method performs search in database and returns an entity with pointed Id.
     *
     * @param entityId - goodsId of searched entity.
     * @return Order searched entity.
     */
    @Override
    @Transactional
    public Optional<Order> get(final Long entityId) {
        return orderRepository.findById(entityId);
    }

    /**
     * Method performs update operation.
     *
     * @param newEntity updated version of entity.
     * @return Order updated version of entity.
     */
    @Override
    @Transactional
    public Order update(final Order newEntity) {
        return orderRepository.save(newEntity);
    }

    /**
     * Method performs delete operation.
     *
     * @param entityId of entity that will be deleted.
     * @return boolean true if entity removed successfully or false if not.
     */
    @Override
    @Transactional
    public boolean delete(final Long entityId) {
        orderRepository.deleteById(entityId);
        return true;
    }

    /**
     * Method returns list of entities that stored in database.
     *
     * @return List list of existed orders.
     */
    @Override
    @Transactional
    public List<Order> getAll() {
        return (List<Order>) orderRepository.findAll();
    }
}




