package com.ra.shop.repository.implementation;

import java.util.List;
import java.util.Optional;

import com.ra.shop.model.Goods;
import com.ra.shop.repository.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUD for Goods.
 */
@Component
public class GoodsRepositoryImpl implements IRepository<Goods> {

    @Autowired
    private transient GoodsRepository goodsRepository;

    /**
     * Create goods in DataBase.
     *
     * @param entity that will be created.
     * @return Entity inserted to database, with added 'ID' from DB.
     */
    @Override
    @Transactional
    public Goods create(final Goods entity) {
        return goodsRepository.save(entity);
    }

    /**
     * Extracted goods in DataBase.
     *
     * @param entityId of entity that will be insert.
     * @return entity.
     */
    @Override
    @Transactional
    public Optional<Goods> get(final Long entityId) {
        return goodsRepository.findById(entityId);
    }

    /**
     * Update goods in DataBase.
     *
     * @param newEntity updated version of entity.
     * @return update entity.
     */
    @Override
    @Transactional
    public Goods update(final Goods newEntity) {
        return goodsRepository.save(newEntity);
    }

    /**
     * Deleting goods in DataBase.
     *
     * @param entityId of entity that will be deleted.
     * @return true else false.
     */
    @Override
    @Transactional
    public boolean delete(final Long entityId) {
        goodsRepository.deleteById(entityId);
        return true;
    }

    /**
     * Extract all goods in DataBase.
     *
     * @return List entity.
     */
    @Override
    @Transactional
    public List<Goods> getAll() {
        return (List<Goods>) goodsRepository.findAll();
    }
}
