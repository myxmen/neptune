package com.ra.shop.repository.implementation;

import com.ra.shop.model.Warehouse;
import com.ra.shop.repository.ShopEntityRepository;

public interface WarehouseRepository extends ShopEntityRepository<Warehouse> {
}
