package com.ra.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity represents user that contains all important information.
 */
@Entity
@Table(name = "`USER`")
@Getter
@Setter
@EqualsAndHashCode(of = {"phoneNumber", "name", "secondName", "country", "emailAddress"})
@ToString(of = {"id", "phoneNumber", "name", "secondName", "country", "emailAddress"})
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column
    private String phoneNumber;

    @Column
    private String name;

    @Column
    private String secondName;

    @Column
    private String country;

    @Column
    private String emailAddress;

}
