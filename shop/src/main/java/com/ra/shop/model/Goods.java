package com.ra.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity goods.
 */
@Entity
@Table(name = "GOODS")
@Getter
@Setter
@EqualsAndHashCode(of = {"name", "barcode", "price"})
@ToString(of = {"id", "name", "barcode", "price"})
@AllArgsConstructor
@NoArgsConstructor
public class Goods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "goods_id")
    private Long id;

    @Column
    private String name;

    @Column
    private Long barcode;

    @Column
    private Double price;

}
