package com.ra.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity represents an order that contains all important information.
 */
@Entity
@Table(name = "`ORDER`")
@Getter
@Setter
@EqualsAndHashCode(of = {"number", "price", "deliveryIncluded", "deliveryCost", "executed"})
@ToString(of = {"id", "number", "price", "deliveryIncluded", "deliveryCost", "executed"})
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;

    @Column
    private Integer number;

    @Column
    private Double price;

    @Column
    private Boolean deliveryIncluded;

    @Column
    private Integer deliveryCost;

    @Column
    private Boolean executed;
}
