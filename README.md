# Welcome to neptune!

![Scheme](images/screen.jpg)

## What is it?

This is my test project **Shop** included into **Neptune** with a backend & a simple frontend.  
**Shop** is Tomcat server's Java application with a database, CRUD, servlets, JSPs, Mosk and Integration Tests.  
This project has been writing step by step from basic to extended technologies.

## Demo
![Scheme](images/show.gif)

## [Here is live demo which has deployed on Heroku.](https://neptune-shop.herokuapp.com/)


## Steps of the project:  
They have links to the relevant branches in the project repository.

* [Start with JDBC](https://bitbucket.org/myxmen/neptune/src/NEP-16-create-entity-goods/)  
* [Migrate project to Spring JDBC](https://bitbucket.org/myxmen/neptune/src/NEP-23-migrate-project-shop-to-spring/) 
* [Add servlets & JSPs](https://bitbucket.org/myxmen/neptune/src/NEP-35-create-servlet-for-shop-project/)
* [Migrate project to Spring MVC](https://bitbucket.org/myxmen/neptune/src/NEP-55-migrate-shop-project-to-spring_mvc/)
* [Migrate project to Hibernate](https://bitbucket.org/myxmen/neptune/src/NEP-56-migrate-shop-project-to-hibernate/)
* [Migrate project to Spring Data](https://bitbucket.org/myxmen/neptune/src/NEP-57-migrate-shop-project-to-spring-data/)
* [Migrate project to PostgreSQL](https://bitbucket.org/myxmen/neptune/src/NEP-58-migrate-shop-project-to-PostgreSQL/)

## Future steps:
* Add Spring Security with OAuth2 
* Migrate project to REST API
* Create a new project by Spring Boot 

# Technologies used in this project

### Programming Languages
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)

### IDEs/Editors
![IntelliJ IDEA](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white)

### Version Control
![Bitbucket](https://img.shields.io/badge/Bitbucket-330F63?style=for-the-badge&logo=bitbucket&logoColor=white)  
![Git](https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=git&logoColor=white)

### Frameworks, Platforms and Libraries
![JDBC](https://img.shields.io/badge/JDBC-%23990000?style=for-the-badge)  
![Servlet](https://img.shields.io/badge/Servlet-%233CAFCE?style=for-the-badge)  
![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![Spring MVC](https://img.shields.io/badge/MVC-SPRING-%236DB33F?style=for-the-badge&logoColor=%236DB33F)
![Spring Data](https://img.shields.io/badge/DATA-SPRING-%236DB33F?style=for-the-badge&logoColor=%236DB33F)  
![Hibernate](https://img.shields.io/static/v1?message=Hibernate&logo=hibernate&color=%2359666c&logoColor=white&style=for-the-badge&label=%20)   

### Databases
![H2](https://img.shields.io/badge/H2-Database-blue?style=for-the-badge)
![PostgreSQL](https://img.shields.io/static/v1?message=PostgreSQL&logo=PostgreSQL&color=%234169E1&logoColor=white&style=for-the-badge&label=%20)

### Logging
![Apache Log4j](https://img.shields.io/badge/Log4j-Apache-%23E0234E?style=for-the-badge)

### Utils
![Lombock](https://img.shields.io/badge/Lombock-%23D40000?style=for-the-badge)

### Build
![Gradle](https://img.shields.io/badge/gradle-02303A?style=for-the-badge&logo=gradle&logoColor=white)

### Testing
![Junit5](https://img.shields.io/badge/Junit5-25A162?style=for-the-badge&logo=junit5&logoColor=white)
![Mockito](https://img.shields.io/badge/Mockito-%2300C244?style=for-the-badge)

### Code analysis
![JaCoCo](https://img.shields.io/badge/JaCoCo-%23721412?style=for-the-badge)
![Checkstyle](https://img.shields.io/badge/Checkstyle-%23FFA500?style=for-the-badge)
![Pmd](https://img.shields.io/badge/Pmd-%23E4202E?style=for-the-badge)

### CI
![CircleCI](https://img.shields.io/badge/CIRCLECI-%23161616.svg?style=for-the-badge&logo=circleci&logoColor=white)

### Web server
![Apache Tomcat](https://img.shields.io/static/v1?message=Apache%20Tomcat&logo=Apache%20Tomcat&color=%23F8DC75&logoColor=black&style=for-the-badge&label=%20)

### Hosting
![Heroku](https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white)

