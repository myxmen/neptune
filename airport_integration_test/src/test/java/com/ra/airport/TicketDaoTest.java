package com.ra.airport;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import com.ra.airport.config.DataBaseConfiguration;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.FlightDao;
import com.ra.airport.repository.impl.TicketDao;
import com.ra.airport.entity.Ticket;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link TicketDao} class
 */
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {DataBaseConfiguration.class})
class TicketDaoTest {

    @Autowired
    TicketDao ticketDao;
    @Autowired
    FlightDao flightDao;
    Ticket ticket;

    @BeforeEach
    public void beforeTest() throws AirPortDaoException {
        ticket = new Ticket(flightDao.getById(1).get(), "KH-1 001", "Petro Velykyi", "AA123456", Timestamp.valueOf("2018-08-30 14:33:08"));
    }

    @Test
    public void whenCreateThenNewTicketWithIdShouldBeReturned() throws AirPortDaoException {
        Ticket createdTicket = ticketDao.create(ticket);
        assertNotNull(createdTicket);
        Integer idTicket = createdTicket.getTicketId();
        assertNotNull(idTicket);
        ticket.setTicketId(idTicket);
        assertEquals(ticket, createdTicket);
        ticketDao.delete(ticket);
    }

    @Test
    public void whenUpdateThenUpdatedTicketShouldBeReturned() throws AirPortDaoException {
        Ticket createdTicket = ticketDao.create(ticket);
        Ticket expectedTicket = changeTicket(createdTicket);
        Ticket updatedTicket = ticketDao.update(createdTicket);
        assertEquals(expectedTicket, updatedTicket);
    }

    @Test
    public void whenDeleteThenDeleteTicketAndReturnTrue() throws AirPortDaoException {
        Ticket createdTicket = ticketDao.create(ticket);
        boolean result = ticketDao.delete(createdTicket);
        assertTrue(result);
    }

    @Test
    public void whenGetTicketByIdThenReturnTicket() throws AirPortDaoException {
        Optional<Ticket> ticket = ticketDao.getById(1);
        assertEquals(ticket.get().getTicketNumber(), this.ticket.getTicketNumber());
    }

    @Test
    public void whenGetAllThenTicketsFromDBShouldBeReturned() throws AirPortDaoException {
        List<Ticket> tickets = ticketDao.getAll();
        assertEquals(tickets.size(), 9);

    }

    private Ticket changeTicket(Ticket ticket) {
        ticket.setTicketNumber("DD111-CC111");
        ticket.setPassengerName("Jane Dow");
        ticket.setDocument("WW12345678WW");
        ticket.setSellingDate(Timestamp.valueOf("2018-07-25 08:00:00"));
        return ticket;
    }
}