package com.ra.airport;

import com.ra.airport.config.DataBaseConfiguration;
import com.ra.airport.entity.Flight;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.AirportDAOImpl;
import com.ra.airport.entity.Airport;
import com.ra.airport.repository.impl.FlightDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {DataBaseConfiguration.class})
class AirportDAOImplTest {

    @Autowired
    AirportDAOImpl airportImpl;
    Airport airport;

    @Autowired
    FlightDao flightDao;

    @BeforeEach
    public void init() {
        airport = new Airport(7,"Kenedy", 12345, "international", "New York", 10);
    }

    @Test
    void whenUpdateAirportThenReturnCreatedAirportWitsId() throws AirPortDaoException {
        airport.setApId(6);
        Airport createdAirport = airportImpl.update(airport);
        airport.setApId(createdAirport.getApId());
        assertEquals(createdAirport, airport);
    }

    @Test
    void whenAddAirportThenReturnCreatedAirportWitsId() throws AirPortDaoException {
        Airport createdAirport = airportImpl.create(airport);
        airport.setApId(createdAirport.getApId());
        assertEquals(createdAirport, airport);
    }

    @Test
    public void whenDeleteAirportThenReturnTrue() throws AirPortDaoException {
        airport.setApId(5);
        assertEquals(airportImpl.delete(airport), true);
    }

    @Test
    public void whenGetAirportByIdThenReturnAirport() throws AirPortDaoException {
        Optional<Airport> ap = airportImpl.getById(1);
        assertEquals(ap.get().getApName(), this.airport.getApName());
        assertEquals(ap.get().getAddress(), this.airport.getAddress());
        assertEquals(ap.get().getApType(), this.airport.getApType());
        assertEquals(ap.get().getApNum(), this.airport.getApNum());
        assertEquals(ap.get().getTerminalCount(), this.airport.getTerminalCount());
    }

    @Test
    public void getAirports() throws AirPortDaoException {
        List<Airport> list = airportImpl.getAll();
        assertEquals(list.size(), 8);
    }
}