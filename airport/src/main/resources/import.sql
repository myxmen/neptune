INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Kenedy', '12345', 'international', 'New York', 10);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Changy', '54367', 'international', 'Singapur', 15);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Shiphol', '32456', 'international', 'Shanhai', 11);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Dallas/fort-uert', '86345', 'international', 'Dallas', 10);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Sharl-de-goll', '14562', 'international', 'Paris', 9);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('Hitrou', '32674', 'international', 'London', 9);
INSERT INTO airport (APNAME, APNUM, APTYPE, ADDRESS, TERMINALCOUNT) VALUES ('O-hara', '83257', 'international', 'Chicago', 7);

INSERT INTO plane (plane_Id, model, type, plate_number, seat_count) VALUES (1, '737 MAX 10', 'Boeing', 1000001, 215);
INSERT INTO plane (plane_Id, model, type, plate_number, seat_count) VALUES (2, '747-8 Intercontinental', 'Boeing', 1000002, 410);
INSERT INTO plane (plane_Id, model, type, plate_number, seat_count) VALUES (3, '757-200', 'Boeing', 1000003, 228);
INSERT INTO plane (plane_Id, model, type, plate_number, seat_count) VALUES (4, '777-300ER', 'Boeing', 1000004, 396);
INSERT INTO plane (plane_Id, model, type, plate_number, seat_count) VALUES (5, '787-10', 'Boeing', 1000005, 330);

INSERT INTO flight (flight_id, plane_id_ref, from_airport_id_ref, to_airport_id_ref, name, carrier, meal_on, fare, departure_date, arrival_date) VALUES (1, 1, 1, 3, 'KH-1', 'American Airlines', 0, 120, '2018-09-04 11:00:00', '2018-09-04 14:00:00');
INSERT INTO flight (flight_id, plane_id_ref, from_airport_id_ref, to_airport_id_ref, name, carrier, meal_on, fare, departure_date, arrival_date) VALUES (2, 2, 2, 3, 'CS-2', 'British Airways', 1, 250, '2018-10-05 08:00:00', '2018-10-05 12:00:00');
INSERT INTO flight (flight_id, plane_id_ref, from_airport_id_ref, to_airport_id_ref, name, carrier, meal_on, fare, departure_date, arrival_date) VALUES (3, 2, 3, 3, 'SD-3', 'Delta Air Lines', 1, 350, '2018-11-24 06:30:00', '2018-11-24 11:30:00');

INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (1, 1, 'KH-1 001', 'Petro Velykyi', 'AA123456', '2018-08-28 14:33:08'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (2, 1, 'KH-1 002', 'Petro Velykyi', 'AA123456', '2018-08-29 14:33:08'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (3, 1, 'KH-1 003', 'Petro Velykyi', 'AA123456', '2018-08-30 14:33:08'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (4, 1, 'CS-2 001', 'Anton Bondar', 'AB123456', '2018-08-28 14:38:48'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (5, 1, 'CS-2 002', 'Anton Bondar', 'AB123456', '2018-08-29 14:38:48'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (6, 1, 'CS-2 003', 'Anton Bondar', 'AB123456', '2018-08-30 14:38:48'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (7, 1, 'SD-3 001', 'Vsevolod Komarnitskiy', 'BB123456', '2018-08-28 14:39:23'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (8, 1, 'SD-3 002', 'Vsevolod Komarnitskiy', 'BB123456', '2018-08-29 14:39:23'),
INSERT INTO ticket (TICKET_ID, FLIGHT_ID_REF, TICKET_NUMBER, PASSENGER_NAME, DOCUMENT, SELLING_DATE) VALUES (9, 1, 'SD-3 003', 'Vsevolod Komarnitskiy', 'BB123456', '2018-08-30 14:39:23');