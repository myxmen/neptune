package com.ra.airport.repository.exception;

/**
 * Exception class for DAO layer.
 */
public class AirPortDaoException extends Exception {

    static final long serialVersionUID = 0L;

    public AirPortDaoException(final String message) {
        super(message);
    }

    public AirPortDaoException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
