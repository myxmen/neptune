package com.ra.airport.repository.impl;

import java.util.List;
import java.util.Optional;

import com.ra.airport.entity.Plane;
import com.ra.airport.repository.AirPortDao;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.exception.ExceptionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PlaneDao implements AirPortDao<Plane> {

    private final transient  SessionFactory sessionFactory;

    private static final Logger LOGGER = LogManager.getLogger(PlaneDao.class);

    @Autowired
    public PlaneDao(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Plane create(final Plane plane) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(plane);
            session.getTransaction().commit();
            return plane;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            LOGGER.error(ExceptionMessage.FAILED_TO_CREATE_NEW_PLANE.toString(), e);
            throw new AirPortDaoException(ExceptionMessage.FAILED_TO_CREATE_NEW_PLANE.get(), e);
        } finally {
            session.close();
        }
    }

    @Override
    public Plane update(final Plane plane) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.update(plane);
            session.getTransaction().commit();
            return plane;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_UPDATE_PLANE_WITH_ID.get() + plane.getPlaneId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    @Override
    public boolean delete(final Plane plane) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(plane);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e)  {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_DELETE_PLANE_WITH_ID.get() + plane.getPlaneId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Plane> getAll() throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return session.createQuery("SELECT pl FROM Plane pl", Plane.class).list();
        } catch (HibernateException e) {
            final String message = ExceptionMessage.FAILED_TO_GET_ALL_PLANES.get();
            LOGGER.error(message, e);
            throw new AirPortDaoException(message, e);
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Plane> getById(final int planeId) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return Optional.ofNullable(session.get(Plane.class, planeId));
        } catch (HibernateException e) {
            final String errorMessage = ExceptionMessage.FAILED_TO_GET_PLANE_WITH_ID.get() + planeId;
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }
}

