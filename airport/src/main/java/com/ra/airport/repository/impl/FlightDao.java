package com.ra.airport.repository.impl;

import java.util.List;
import java.util.Optional;

import com.ra.airport.entity.Flight;
import com.ra.airport.repository.AirPortDao;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.exception.ExceptionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Implementation of {@link AirPortDao} interface.
 */
@Repository
public class FlightDao implements AirPortDao<Flight> {

    private static final Logger LOGGER = LogManager.getLogger(FlightDao.class);

    private final transient  SessionFactory sessionFactory;

    @Autowired
    public FlightDao(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create {@link Flight} entity in DB and return it.
     *
     * @param flight entity to create
     * @return {@link Flight} entity
     * @throws AirPortDaoException exception for DAO layer
     */
    public Flight create(final Flight flight) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(flight);
            session.getTransaction().commit();
            return flight;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            LOGGER.error(ExceptionMessage.FAILED_TO_CREATE_NEW_FLIGHT.toString(), e);
            throw new AirPortDaoException(ExceptionMessage.FAILED_TO_CREATE_NEW_FLIGHT.get(), e);
        } finally {
            session.close();
        }
    }

    /**
     * Update {@link Flight} entity in DB and return it.
     *
     * @param flight entity to update
     * @return {@link Flight} entity
     * @throws AirPortDaoException exception for DAO layer
     */
    public Flight update(final Flight flight) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.update(flight);
            session.getTransaction().commit();
            return flight;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_UPDATE_FLIGHT_WITH_ID.get() + flight.getFlId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Delete {@link Flight} entity in DB.
     * And return true if operation was successful or false if not.
     *
     * @param flight entity to delete
     * @return boolean flag
     * @throws AirPortDaoException exception for DAO layer
     */
    public boolean delete(final Flight flight) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(flight);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_DELETE_FLIGHT_WITH_ID.get() + flight.getFlId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Return {@link Flight} object from DB or exception if object not exists.
     *
     * @param flightId flight Id
     * @return {@link Flight}
     */
    @Override
    public Optional<Flight> getById(final int flightId) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return Optional.ofNullable(session.get(Flight.class, flightId));
        } catch (HibernateException e) {
            final String errorMessage = ExceptionMessage.FAILED_TO_GET_FLIGHT_WITH_ID.get() + flightId;
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Return all entities from DB by {@link Flight} type.
     * If entities absent in DB return empty {@link List}.
     *
     * @return List entities
     * @throws AirPortDaoException exception for DAO layer
     */
    @Override
    public List<Flight> getAll() throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return session.createQuery("SELECT fl FROM Flight fl", Flight.class).list();
        } catch (HibernateException e) {
            final String message = ExceptionMessage.FAILED_TO_GET_ALL_FLIGHTS.get();
            LOGGER.error(message, e);
            throw new AirPortDaoException(message, e);
        } finally {
            session.close();
        }
    }
}