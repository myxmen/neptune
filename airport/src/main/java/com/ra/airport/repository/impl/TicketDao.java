package com.ra.airport.repository.impl;

import java.util.List;
import java.util.Optional;

import com.ra.airport.entity.Ticket;
import com.ra.airport.repository.AirPortDao;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.exception.ExceptionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Implementation of {@link AirPortDao} interface.
 */
@Repository
public class TicketDao implements AirPortDao<Ticket> {

    private static final Logger LOGGER = LogManager.getLogger(TicketDao.class);

    private final transient SessionFactory sessionFactory;

    @Autowired
    public TicketDao(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Create {@link Ticket} entity in DB and return it.
     *
     * @param ticket entity to create
     * @return {@link Ticket} entity
     * @throws AirPortDaoException exception for DAO layer
     */
    @Override
    public Ticket create(final Ticket ticket) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(ticket);
            session.getTransaction().commit();
            return ticket;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            LOGGER.error(ExceptionMessage.FAILED_TO_CREATE_NEW_TICKET.toString(), e);
            throw new AirPortDaoException(ExceptionMessage.FAILED_TO_CREATE_NEW_TICKET.get(), e);
        } finally {
            session.close();
        }
    }

    /**
     * Update {@link Ticket} entity in DB and return it.
     *
     * @param ticket entity to update
     * @return {@link Ticket} entity
     * @throws AirPortDaoException exception for DAO layer
     */
    @Override
    public Ticket update(final Ticket ticket) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.update(ticket);
            session.getTransaction().commit();
            return ticket;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_UPDATE_TICKET_WITH_ID.get() + ticket.getTicketId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Delete {@link Ticket} entity in DB.
     * And return true if operation was successful or false if not.
     *
     * @param ticket entity to delete
     * @return boolean flag
     * @throws AirPortDaoException exception for DAO layer
     */
    @Override
    public boolean delete(final Ticket ticket) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(ticket);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_DELETE_TICKET_WITH_ID.get() + ticket.getTicketId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Return {@link Ticket} object from DB or exception if object not exists.
     *
     * @param ticketId ticket Id
     * @return {@link Ticket}
     */
    @Override
    public Optional<Ticket> getById(final int ticketId) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return Optional.ofNullable(session.get(Ticket.class, ticketId));
         } catch (HibernateException e) {
            final String errorMessage = ExceptionMessage.FAILED_TO_GET_TICKET_WITH_ID.get() + ticketId;
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    /**
     * Return all entities from DB by {@link Ticket} type.
     * If entities absent in DB return empty {@link List}.
     *
     * @return List entities
     * @throws AirPortDaoException exception for DAO layer
     */
    @Override
    public List<Ticket> getAll() throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return session.createQuery("SELECT tic FROM Ticket tic", Ticket.class).list();
        } catch (HibernateException e) {
            final String message = ExceptionMessage.FAILED_TO_GET_ALL_TICKETS.get();
            LOGGER.error(message, e);
            throw new AirPortDaoException(message, e);
        } finally {
            session.close();
        }
    }
}
