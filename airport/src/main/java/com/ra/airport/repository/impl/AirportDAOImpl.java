package com.ra.airport.repository.impl;

import java.util.List;
import java.util.Optional;

import com.ra.airport.entity.Airport;
import com.ra.airport.repository.AirPortDao;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.exception.ExceptionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AirportDAOImpl implements AirPortDao<Airport> {

    private final transient SessionFactory sessionFactory;

    private static final Logger LOGGER = LogManager.getLogger(AirportDAOImpl.class);

    @Autowired
    public AirportDAOImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Airport create(final Airport airport) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(airport);
            session.getTransaction().commit();
            return airport;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_CREATE_NEW_AIRPORT.get() + airport.getApId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }

    }

    @Override
    public Airport update(final Airport airport) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.update(airport);
            session.getTransaction().commit();
            return airport;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_UPDATE_AIRPORT_WITH_ID.get() + airport.getApId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }

    }

    @Override
    public boolean delete(final Airport airport) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(airport);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            final String errorMessage = ExceptionMessage.FAILED_TO_DELETE_AIRPORT_WITH_ID.get() + airport.getApId();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Airport> getById(final int airportId) throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return Optional.ofNullable(session.get(Airport.class, airportId));
        } catch (HibernateException e) {
            final String errorMessage = ExceptionMessage.FAILED_TO_GET_AIRPORT_WITH_ID.get() + airportId;
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Airport> getAll() throws AirPortDaoException {
        final Session session = sessionFactory.openSession();
        try {
            return session.createQuery("SELECT air FROM Airport air", Airport.class).list();
        } catch (HibernateException e) {
            final String errorMessage = ExceptionMessage.FAILED_TO_GET_ALL_AIRPORTS.get();
            LOGGER.error(errorMessage, e);
            throw new AirPortDaoException(errorMessage, e);
        } finally {
            session.close();
        }
    }
}
