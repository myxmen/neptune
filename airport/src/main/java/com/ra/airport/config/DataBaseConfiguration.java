package com.ra.airport.config;

import java.util.Properties;

import com.ra.airport.entity.Airport;
import com.ra.airport.entity.Flight;
import com.ra.airport.entity.Plane;
import com.ra.airport.entity.Ticket;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.H2Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

/**
 * Spring configuration class for DAO layer.
 */
@PropertySource("classpath:config.properties")
@ComponentScan("com.ra.airport")
@Configuration
public class DataBaseConfiguration {

    @Autowired
    private transient Environment environment;

    /**
     * Register {@link LocalSessionFactoryBean} bean.
     *
     * @return sessionFactoryBean
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        final LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setHibernateProperties(hibernateProperties());
        factoryBean.setAnnotatedClasses(Airport.class, Flight.class, Ticket.class, Plane.class);
        return factoryBean;
    }

    private Properties hibernateProperties() {

        final Properties properties = new Properties();
        properties.setProperty(AvailableSettings.URL, environment.getProperty("jdbc.url"));
        properties.setProperty(AvailableSettings.USER, environment.getProperty("jdbc.user"));
        properties.setProperty(AvailableSettings.PASS, environment.getProperty("jdbc.password"));
        properties.setProperty(AvailableSettings.DIALECT, H2Dialect.class.getName());
        properties.setProperty(AvailableSettings.SHOW_SQL, String.valueOf(true));
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO, "create");
        properties.setProperty(AvailableSettings.DRIVER, environment.getProperty("jdbc.driverClassName"));
        return properties;
    }
}
