package com.ra.airport.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Flight entity correspond to flight table in DB.
 */
@Entity
@Table(name = "FLIGHT")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flight_id")
    private Integer flId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "plane_id_ref")
    private Plane planeIdRef;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "from_airport_id_ref")
    private Airport fromAirportIdRef;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_airport_id_ref")
    private Airport toAirportIdRef;

    @Column(name = "name")
    private String name;

    @Column(name = "carrier")
    private String carrier;

    @Column(name = "departure_date")
    private LocalDateTime departureDate;

    @Column(name = "arrival_date")
    private LocalDateTime arrivalDate;

    @Column(name = "fare")
    private Double fare;

    @Column(name = "meal_on")
    private Boolean mealOn;

    public Flight() {}

    public Integer getFlId() {
        return flId;
    }

    public void setFlId(final Integer flId) {
        this.flId = flId;
    }

    public Plane getPlaneIdRef() {
        return planeIdRef;
    }

    public void setPlaneIdRef(final Plane planeIdRef) {
        this.planeIdRef = planeIdRef;
    }

    public Airport getFromAirportIdRef() {
        return fromAirportIdRef;
    }

    public void setFromAirportIdRef(final Airport fromAirportIdRef) {
        this.fromAirportIdRef = fromAirportIdRef;
    }

    public Airport getToAirportIdRef() {
        return toAirportIdRef;
    }

    public void setToAirportIdRef(final Airport toAirportIdRef) {
        this.toAirportIdRef = toAirportIdRef;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(final LocalDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(final LocalDateTime arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Double getFare() {
        return fare;
    }

    public void setFare(final Double fare) {
        this.fare = fare;
    }

    public Boolean getMealOn() {
        return mealOn;
    }

    public void setMealOn(final Boolean mealOn) {
        this.mealOn = mealOn;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final Flight flight = (Flight) object;
        return Objects.equals(flId, flight.flId)
                && Objects.equals(name, flight.name)
                && Objects.equals(carrier, flight.carrier)
                && Objects.equals(departureDate, flight.departureDate)
                && Objects.equals(arrivalDate, flight.arrivalDate)
                && Objects.equals(fare, flight.fare)
                && Objects.equals(mealOn, flight.mealOn);
    }

    @Override
    public String toString() {
        return "Flight{"
                + "id=" + flId
                + ", name='" + name + '\''
                + ", carrier='" + carrier + '\''
                + ", departureDate=" + departureDate
                + ", arrivalDate=" + arrivalDate
                + ", fare=" + fare
                + ", mealOn=" + mealOn
                + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(flId, name, carrier, departureDate, arrivalDate, fare, mealOn);
    }
}
