package com.ra.airport.entity;

import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TICKET")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Integer ticketId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "flight_id_ref")
    private Flight flightIdRef;
    @Column(name = "ticket_number")
    private String ticketNumber;
    @Column(name = "passenger_name")
    private String passengerName;
    @Column(name = "document")
    private String document;
    @Column(name = "selling_date")
    private Timestamp sellingDate;

    public Ticket() {
    }

    public Ticket(final Flight flightIdRef, final String ticketNumber, final String passengerName, final String document,
                  final Timestamp sellingDate) {
        this.flightIdRef = flightIdRef;
        this.ticketNumber = ticketNumber;
        this.passengerName = passengerName;
        this.document = document;
        this.sellingDate = sellingDate;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(final Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Flight getFlightIdRef() {
        return flightIdRef;
    }

    public void setFlightIdRef(final Flight flightIdRef) {
        this.flightIdRef = flightIdRef;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(final String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(final String passengerName) {
        this.passengerName = passengerName;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(final String document) {
        this.document = document;
    }

    public Timestamp getSellingDate() {
        return sellingDate;
    }

    public void setSellingDate(final Timestamp sellingDate) {
        this.sellingDate = sellingDate;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final Ticket ticket = (Ticket) object;
        return Objects.equals(ticketId, ticket.ticketId)
                && Objects.equals(flightIdRef, ticket.flightIdRef)
                && Objects.equals(ticketNumber, ticket.ticketNumber)
                && Objects.equals(passengerName, ticket.passengerName)
                && Objects.equals(document, ticket.document)
                && Objects.equals(sellingDate, ticket.sellingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, flightIdRef, ticketNumber, passengerName, document, sellingDate);
    }

    @Override
    public String toString() {
        return "Ticket{"
                + "ticketId=" + ticketId
                + ", flightIdRef=" + flightIdRef
                + ", ticketNumber='" + ticketNumber + '\''
                + ", passengerName='" + passengerName + '\''
                + ", document='" + document + '\''
                + ", sellingDate=" + sellingDate
                + '}';
    }
}
