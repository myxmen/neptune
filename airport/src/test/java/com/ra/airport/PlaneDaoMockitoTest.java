package com.ra.airport;

import com.ra.airport.entity.Airport;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.PlaneDao;
import com.ra.airport.entity.Plane;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import static com.ra.airport.repository.exception.ExceptionMessage.*;
import static org.junit.jupiter.api.Assertions.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.when;

public class PlaneDaoMockitoTest {

    @Mock
    Query query;
    @Mock
    Session session;
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Transaction transaction;
    @InjectMocks
    private PlaneDao planeDao;
    private Plane plane;

    @BeforeEach
    public void init() throws SQLException {
        MockitoAnnotations.initMocks(this);
        plane = createPlane();
        Mockito.when(sessionFactory.openSession()).thenReturn(session);
        Mockito.when(session.getTransaction()).thenReturn(transaction);
    }

    private Plane createPlane() {
        Plane plane = new Plane();
        plane.setPlaneId(1);
        plane.setPlateNumber(2);
        plane.setModel("test");
        plane.setType("test");
        plane.setSeatsCount(150);
        return plane;
    }

    @Test
    public void whenCreateThenCorrectSQLShouldBeExecutedAndCorrectEntityShouldBeReturned() throws AirPortDaoException {
        Plane result = planeDao.create(plane);
        assertEquals(plane, result);
    }

    @Test
    public void whenUpdateThenCorrectSQLShouldBeExecutedAndCorrectEntityShouldBeReturned() throws AirPortDaoException {
        Plane result = planeDao.update(plane);
        assertEquals(plane, result);
    }

    @Test
    public void whenDeleteThenCorrectSQLShouldBeExecutedAndTrueShouldBeReturned() throws AirPortDaoException{
        boolean result = planeDao.delete(plane);
        assertEquals(true, result);
    }

    @Test
    public void whenGetAllThenCorrectSQLShouldBeExecutedAndCorrectListReturned() throws AirPortDaoException {
        List<Airport> list = new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        };
        Mockito.when(session.createQuery("SELECT pl FROM Plane pl", Plane.class)).thenReturn(query);
        Mockito.when(query.list()).thenReturn(list);
        planeDao.getAll();
        assertEquals(list.size(), 3);
    }

    @Test
    public void whenGetByIdReturnEmptyResultSetThenEmptyOptionalShouldBeReturned() throws AirPortDaoException {
        Mockito.when(session.get(Plane.class, 1)).thenReturn(plane);
        Optional<Plane> findPlane = planeDao.getById(1);
        assertEquals(findPlane.get(), plane);
    }

    @Test
    public void whenCreateThrownEmptyResultDataAccessExceptionThenDAOExceptionShouldBeThrownToo() {
        when(session.save(plane)).thenThrow(HibernateException.class);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            planeDao.create(plane);
        });
        assertNotNull(exception.getMessage(), FAILED_TO_CREATE_NEW_PLANE.get());
    }

    @Test
    public void whenUpdateThrownEmptyResultDataAccessExceptionThenDAOExceptionShouldBeThrownToo() {
        Mockito.doThrow(HibernateException.class).when(session).update(plane);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            planeDao.update(plane);
        });
        assertEquals(exception.getMessage(), FAILED_TO_UPDATE_PLANE_WITH_ID.get()+1);
    }

    @Test
    public void whenDeleteThrownEmptyResultDataAccessExceptionThenDAOExceptionShouldBeThrownToo() {
        Mockito.doThrow(HibernateException.class).when(session).delete(plane);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            planeDao.delete(plane);
        });
        assertEquals(exception.getMessage(), FAILED_TO_DELETE_PLANE_WITH_ID.get()+1);
    }

    @Test
    public void whenGetAllThrownEmptyResultDataAccessExceptionThenDAOExceptionShouldBeThrownToo() {
        Mockito.doThrow(HibernateException.class).when(session).createQuery("SELECT pl FROM Plane pl", Plane.class);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            planeDao.getAll();
        });
        assertEquals(exception.getMessage(), FAILED_TO_GET_ALL_PLANES.get());
    }

    @Test
    public void whenGetByIdThrownEmptyResultDataAccessExceptionThenDAOExceptionShouldBeThrownToo() {
        Mockito.doThrow(HibernateException.class).when(session).get(Plane.class, 1);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            planeDao.getById(1);
        });
        assertEquals(exception.getMessage(), FAILED_TO_GET_PLANE_WITH_ID.get()+1);
    }
}


