package com.ra.airport;


import com.ra.airport.entity.Flight;
import com.ra.airport.entity.Ticket;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.TicketDao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Mockito tests for {@link TicketDao} class
 */
public class TicketDaoMockitoTest {
    @Mock
    Query query;
    @Mock
    Session session;
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Transaction transaction;
    @InjectMocks
    private TicketDao ticketDAO;
    private Ticket ticket;
    private Flight flight;

    @BeforeEach
    public void init() throws SQLException {
        MockitoAnnotations.initMocks(this);
        flight = new Flight();
        flight.setFlId(1);
        ticket = new Ticket(flight, "KH-1 001", "Petro Velykyi", "AA123456", Timestamp.valueOf("2018-08-30 14:33:08"));
        Mockito.when(sessionFactory.openSession()).thenReturn(session);
        Mockito.when(session.getTransaction()).thenReturn(transaction);
    }

    @Test
    public void whenCreateTicketThenReturnTicketWithIdTrue() throws AirPortDaoException {
        Ticket createdTicket = ticketDAO.create(ticket);
        assertEquals(createdTicket, ticket);
    }

    @Test
    public void whenCreateTicketThenThrowExseption() {
        Mockito.when(session.save(Mockito.any())).thenThrow(HibernateException.class);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            ticketDAO.create(ticket);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenUpdateTicketThenReturnTicket() throws AirPortDaoException {
        Ticket updatedTicket = ticketDAO.update(ticket);
        assertEquals(updatedTicket, ticket);
    }

    @Test
    public void whenUpdateTicketThenThrowException() {
        Mockito.doThrow(HibernateException.class).when(session).update(Mockito.any());
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            ticketDAO.update(ticket);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenDeleteTicketThenReturnTrue() throws AirPortDaoException {
        boolean createdTicket = ticketDAO.delete(ticket);
        assertEquals(createdTicket, true);
    }

    @Test
    public void whenDeleteTicketThenThrowException() {
        Mockito.doThrow(HibernateException.class).when(session).delete(Mockito.any());
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            ticketDAO.delete(ticket);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenGetByIdTicketThenReturnTicket() throws AirPortDaoException {
        Mockito.when(session.get(Ticket.class, 1)).thenReturn(ticket);
        Ticket findTicket = ticketDAO.getById(1).get();
        assertEquals(findTicket, ticket);
    }

    @Test
    public void whenGetByIdTicketThenThrowException() {
        Mockito.doThrow(HibernateException.class).when(session).get(Ticket.class, 1);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            ticketDAO.getById(1);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenGetAllThenReturnList() throws AirPortDaoException {
        List<Ticket> list = new ArrayList<>() {
            @Override
            public int size() {
                return 9;
            }
        };
        Mockito.when(session.createQuery("SELECT tic FROM Ticket tic", Ticket.class)).thenReturn(query);
        Mockito.when(query.list()).thenReturn(list);
        ticketDAO.getAll();
        assertEquals(list.size(), 9);
    }

    @Test
    public void whenGetAllThenThrowException() {
        Mockito.when(session.createQuery("SELECT tic FROM Ticket tic", Ticket.class)).thenThrow(HibernateException.class);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            ticketDAO.getAll();
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void throwNewAirPortDaoException() {
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            throw new AirPortDaoException("test Exception");
        });
        assertNotNull(thrown.getMessage());
    }

}
