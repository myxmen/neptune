package com.ra.airport;

import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.AirportDAOImpl;
import com.ra.airport.entity.Airport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import java.sql.SQLException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class AirportDAOImplMockitoTest {

    @Mock
    Query query;
    @Mock
    Session session;
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Transaction transaction;
    @InjectMocks
    private AirportDAOImpl airportDAO;
    private Airport airport;

    @BeforeEach
    public void init() throws SQLException {
        MockitoAnnotations.initMocks(this);
        airport = new Airport(8,"Kenedy", 4949034, "International", "USA New Yourk", 10);
        Mockito.when(sessionFactory.openSession()).thenReturn(session);
        Mockito.when(session.getTransaction()).thenReturn(transaction);
    }

    @Test
    public void whenCreateAirportThenReturnAirportWitsIdTrue() throws AirPortDaoException {
        Airport createdAirport = airportDAO.create(airport);
        assertEquals(createdAirport, airport);
    }

    @Test
    public void whenCreateAirportThenThrowExseption() {
        Mockito.when(session.save(Mockito.any())).thenThrow(HibernateException.class);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            airportDAO.create(airport);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenUpdateAirportThenReturnAirport() throws AirPortDaoException {
        Airport updatedAirport = airportDAO.update(airport);
        assertEquals(updatedAirport, airport);
    }

    @Test
    public void whenUpdateAirportThenThrowExseption() {
        Mockito.doThrow(HibernateException.class).when(session).update(Mockito.any());
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            airportDAO.update(airport);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenDeleteAirportThenReturnTrue() throws AirPortDaoException {
        boolean createdAirport = airportDAO.delete(airport);
        assertEquals(createdAirport, true);
    }

    @Test
    public void whenDeleteAirportThenThrowExseption() {
        Mockito.doThrow(HibernateException.class).when(session).delete(Mockito.any());
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            airportDAO.delete(airport);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenGetByIdAirportThenReturnAirport() throws AirPortDaoException {
        Mockito.when(session.get(Airport.class, 1)).thenReturn(airport);
        Airport findAirport = airportDAO.getById(1).get();
        assertEquals(findAirport, airport);
    }

    @Test
    public void whenGetByIdAirportThenThrowExseption() {
        Mockito.doThrow(HibernateException.class).when(session).get(Airport.class, 1);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            airportDAO.getById(1);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void whenGetAllThenReturnList() throws AirPortDaoException {
        List<Airport> list = new ArrayList<>() {
            @Override
            public int size() {
                return 3;
            }
        };
        Mockito.when(session.createQuery("SELECT air FROM Airport air", Airport.class)).thenReturn(query);
        Mockito.when(query.list()).thenReturn(list);
        airportDAO.getAll();
        assertEquals(list.size(), 3);
    }

    @Test
    public void whenGetAllThenThrowExseption() {
        Mockito.when(session.createQuery("SELECT air FROM Airport air", Airport.class)).thenThrow(HibernateException.class);
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            airportDAO.getAll();
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    public void throwNewAirPortDaoException() {
        Throwable thrown = assertThrows(AirPortDaoException.class, () -> {
            throw new AirPortDaoException("test Exception");
        });
        assertNotNull(thrown.getMessage());
    }
}