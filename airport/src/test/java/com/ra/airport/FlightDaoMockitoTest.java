package com.ra.airport;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import com.ra.airport.entity.Flight;
import com.ra.airport.repository.exception.AirPortDaoException;
import com.ra.airport.repository.impl.FlightDao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static com.ra.airport.repository.exception.ExceptionMessage.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

/**
 * Mockito tests for {@link FlightDao} class
 */

public class FlightDaoMockitoTest {

    @InjectMocks
    private FlightDao flightDao;

    private Flight flight;

    private List<Flight> flights;

    @Mock
    private Query query;

    @Mock
    private SessionFactory sessionFactory;

    @Mock
    private Session session;

    @Mock
    private Transaction transaction;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        createFlight();
        flights = new ArrayList<>(Arrays.asList(flight));

        when(sessionFactory.openSession()).thenReturn(session);
        when(session.get(Flight.class, 1)).thenReturn(flight);
        when(session.createQuery("SELECT fl FROM Flight fl", Flight.class)).thenReturn(query);
        when(query.list()).thenReturn(flights);
        when(session.getTransaction()).thenReturn(transaction);
    }

    @Test
    public void whenCreateThenCorrectEntityShouldBeReturned() throws AirPortDaoException {
        Flight result = flightDao.create(this.flight);

        assertEquals(flight, result);
    }

    @Test
    public void whenUpdateThenCorrectEntityShouldBeReturned() throws AirPortDaoException {
        Flight result = flightDao.update(flight);

        assertEquals(flight, result);
    }

    @Test
    public void whenDeleteThenTrueShouldBeReturned() throws AirPortDaoException {
        boolean result = flightDao.delete(flight);

        assertTrue(result);
    }

    @Test
    public void whenGetByIdThenOptionalOfFlightShouldBeReturned() throws AirPortDaoException {
        Optional<Flight> result = flightDao.getById(1);

        assertEquals(Optional.of(flight), result);
    }

    @Test
    public void whenGetAllThenListOfFlightsShouldBeReturned() throws AirPortDaoException {
        List<Flight> result = flightDao.getAll();

        assertEquals(flights, result);
    }

    @Test
    public void whenCreateThrownHibernateExceptionThenDAOExceptionShouldBeThrownToo() {
        when(session.save(flight)).thenThrow(HibernateException.class);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            flightDao.create(flight);
        });

        assertEquals(exception.getMessage(), FAILED_TO_CREATE_NEW_FLIGHT.get());
    }

    @Test
    public void whenUpdateThrownHibernateExceptionThenDAOExceptionShouldBeThrownToo() {
        doThrow(HibernateException.class).when(session).update(flight);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            flightDao.update(flight);
        });

        assertEquals(exception.getMessage(), FAILED_TO_UPDATE_FLIGHT_WITH_ID.get() + 1);
    }

    @Test
    public void whenDeleteThrownHibernateExceptionThenDAOExceptionShouldBeThrownToo() {
        doThrow(HibernateException.class).when(session).delete(flight);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            flightDao.delete(flight);
        });

        assertEquals(exception.getMessage(), FAILED_TO_DELETE_FLIGHT_WITH_ID.get() + 1);
    }

    @Test
    public void whenGetAllThrownHibernateExceptionThenDAOExceptionShouldBeThrownToo() {
        doThrow(HibernateException.class).when(session).createQuery("SELECT fl FROM Flight fl", Flight.class);
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            flightDao.getAll();
        });

        assertEquals(exception.getMessage(), FAILED_TO_GET_ALL_FLIGHTS.get());
    }

    @Test
    public void whenGetByIdThrownHibernateExceptionThenDAOExceptionShouldBeThrownToo() {
        doThrow(HibernateException.class).when(session).get(Flight.class, flight.getFlId());
        Throwable exception = assertThrows(AirPortDaoException.class, () -> {
            flightDao.getById(flight.getFlId());
        });

        assertEquals(exception.getMessage(), FAILED_TO_GET_FLIGHT_WITH_ID.get() + 1);
    }

    public void createFlight() {
        flight = new Flight();
        flight.setFlId(1);
        flight.setName(" ");
        flight.setCarrier(" ");
        flight.setFare(Double.MIN_VALUE);
        flight.setMealOn(true);
        flight.setDepartureDate(LocalDateTime.MIN);
        flight.setArrivalDate(LocalDateTime.MAX);
    }
}
