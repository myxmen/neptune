package com.ra.shop.repository;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.model.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class OrderRepositoryIntegrationTest {

    @Autowired
    private IRepository<Order> repository;

    private static final Order TEST_ORDER = new Order(null, 10, 90d, false,
            0, false);
    private static final Order TEST_ORDER_UPDATE = new Order(null, 10, 900d, true,
            120, false);

    @BeforeEach
    void delete() {
        repository.getAll().stream().forEach(i -> {
            repository.delete(i.getId());
        });
        TEST_ORDER.setId(null);
    }

    @Test
    void whenCreateOrderThenReturnCreatedOrder() {
        Order created = repository.create(TEST_ORDER);
        assertEquals(TEST_ORDER, created);
    }

    @Test
    void whenGetOrderThenReturnCorrectEntity() {
        assertEquals(TEST_ORDER, repository.create(TEST_ORDER));
    }

    @Test
    void whenUpdateOrderThenReturnUpdatedOrder() {
        Order created = repository.create(TEST_ORDER);
        TEST_ORDER_UPDATE.setId((created.getId()));
        Order updated = repository.update(TEST_ORDER_UPDATE);
        assertAll(() -> {
            assertEquals(TEST_ORDER_UPDATE.getPrice(), updated.getPrice());
            assertEquals(TEST_ORDER_UPDATE.getDeliveryIncluded(), updated.getDeliveryIncluded());
            assertEquals(TEST_ORDER_UPDATE.getDeliveryCost(), updated.getDeliveryCost());
        });
    }

    @Test
    void whenDeleteOrderThenReturnTrueOnSuccessfulExecution() {
        Order created = repository.create(TEST_ORDER);
        assertTrue(repository.delete(created.getId()));
    }

    @Test
    void whenGetAllOrdersThenReturnListOfOrders() {
        Order[] orders = getOrders();
        addAllOrdersToDB(orders);
        assertEquals(orders.length, repository.getAll().size());
    }

    private void addAllOrdersToDB(Order[] orders) {
        for (int i = 0; i < orders.length; i++) {
            repository.create(orders[i]);
        }
    }

    private Order[] getOrders() {
        return new Order[]{
                new Order(null, 110, 390d, true, 30, false),
                new Order(null, 210, 490d, true, 50, true),
                new Order(null, 310, 590d, false, 0, true)
        };
    }
}