package com.ra.shop.repository;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.model.Warehouse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ShopConfiguration.class)
@WebAppConfiguration
public class WarehouseRepositoryImplTest {

    @Autowired
    private IRepository<Warehouse> warehouseRepository;

    private static Warehouse warehouse;

    @BeforeAll
    static void init() {
        warehouse = new Warehouse(null, "Lol", 1.1, 2);
    }

    @BeforeEach
    void delete() {
        warehouseRepository.getAll().stream().forEach(i -> {
            warehouseRepository.delete(i.getId());
        });
        warehouse.setId(null);
    }

    @Test
    void whenCreateTableThenNewWarehouseMustReturn() {
        warehouseRepository.create(warehouse);
        assertEquals((warehouseRepository.getAll().get(warehouseRepository.getAll().size() - 1)).getId(),
                warehouse.getId());
    }

    @Test
    void whenUpdateThenUpdatedWarehouseReturns() {
        Warehouse expectedWarehouse = warehouseRepository.create(warehouse);
        expectedWarehouse.setName("AloGarage");
        assertEquals(expectedWarehouse, warehouseRepository.update(expectedWarehouse));
    }

    @Test
    public void whenGetWarehouseEqualsCreatedWarehouse() {
        assertTrue(warehouseRepository.create(warehouse).equals(warehouseRepository.get(warehouse.getId()).get()));
    }

    @Test
    void whenDeleteCorrectlyThenDeleteAndReturnTrue() {
        Warehouse createdWarehouse1 = warehouseRepository.create(warehouse);
        assertTrue(warehouseRepository.delete(createdWarehouse1.getId()));
    }

    @Test
    void whenGetAllThenWarehousesMustReturn() {
        Warehouse[] warehouses = getWarehouses();
        List<Warehouse> expectedList = new ArrayList<>();
        Collections.addAll(expectedList, warehouses);
        addWarehouses(warehouses);
        assertEquals(expectedList.size(), warehouseRepository.getAll().size());
    }

    private void addWarehouses(Warehouse[] warehouses) {
        for (int i = 0; i < warehouses.length; i++) {
            warehouseRepository.create(warehouses[i]);
        }
    }

    private Warehouse[] getWarehouses() {
        return new Warehouse[]{
                new Warehouse(null, "loha", 1.1, 1),
                new Warehouse(null, "gosha", 1.1, 1),
                new Warehouse(null, "moisha", 1.1, 1)
        };
    }
}
