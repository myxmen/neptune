package com.ra.shop.repository;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.model.Goods;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class GoodsRepositoryIntegrationTest {

    private static final Goods TEST_GOODS = new Goods(null, "Camel", 7622210609779l, 1.2d);
    private static final Goods TEST_GOODS_UPDATE = new Goods(null, "Marlboro", 7622210721516l, 5.7d);

    @Autowired
    private IRepository<Goods> dao;

    @BeforeEach
    void delete() {
        dao.getAll().stream().forEach(i -> {
            dao.delete(i.getId());
        });
        TEST_GOODS.setId(null);
    }

    @Test
    public void whenCreatedGoodEqualsGoods() {
        assertTrue(dao.create(TEST_GOODS).equals(TEST_GOODS));
    }

    @Test
    public void whenGetGoodsEqualsCreatedGoods() {
        assertTrue(dao.create(TEST_GOODS).equals(dao.get(TEST_GOODS.getId()).get()));
    }

    @Test
    public void whenDeletingCreatedGoodsReturnTrue() {
        assertTrue(dao.delete((dao.create(TEST_GOODS)).getId()));
    }

    @Test
    public void whenUpdatingCreatedGoodsEqualsUpdatingGoods() {
        TEST_GOODS_UPDATE.setId(dao.create(TEST_GOODS).getId());
        assertTrue(dao.update(TEST_GOODS_UPDATE).equals(dao.get(TEST_GOODS_UPDATE.getId()).get()));
    }

    @Test
    public void whenReadAllGoodsReturnEmptyList() {
        dao.create(TEST_GOODS).getId();
        assertFalse(dao.getAll().isEmpty());
    }
}
