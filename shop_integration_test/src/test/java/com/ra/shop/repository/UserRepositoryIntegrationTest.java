package com.ra.shop.repository;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class UserRepositoryIntegrationTest {

    @Autowired
    private IRepository<User> repository;

    private static final User TEST_USER = new User(null, "3809978957860", "Pasha", "Vakula",
            "Poland", "vakula_2123@gmail.com");
    private static final User TEST_USER_UPDATE = new User(null, "3809978957860", "Gugulya", "Zahrema",
            "Turkey", "vakula_2123@gmail.com");

    @BeforeEach
    void delete() {
        repository.getAll().stream().forEach(i -> {
            repository.delete(i.getId());
        });
        TEST_USER.setId(null);
    }

    @Test
    void whenCreateUserThenReturnCreatedUser() {

        assertEquals(TEST_USER, repository.create(TEST_USER));
    }

    @Test
    void whenGetUserThenReturnCorrectUser() {
        assertEquals(TEST_USER, repository.create(TEST_USER));
    }

    @Test
    void whenUpdateUserThenReturnUpdatedUser() {
        User created = repository.create(TEST_USER);
        TEST_USER_UPDATE.setId(created.getId());
        User updated = repository.update(TEST_USER_UPDATE);
        assertAll(() -> {
            assertEquals(TEST_USER_UPDATE.getName(), updated.getName());
            assertEquals(TEST_USER_UPDATE.getSecondName(), updated.getSecondName());
            assertEquals(TEST_USER_UPDATE.getCountry(), updated.getCountry());
        });
    }

    @Test
    void whenDeleteUserThenReturnTrueOnSuccessfulExecution() {
        User created = repository.create(TEST_USER);
        assertTrue(repository.delete(created.getId()));
    }

    @Test
    void whenGetAllUsersThenReturnListOfUsers() {
        User[] users = getUsers();
        addAllUsersToDB(users);
        assertEquals(users.length, repository.getAll().size());
    }

    private void addAllUsersToDB(User[] users) {
        for (int i = 0; i < users.length; i++) {
            repository.create(users[i]);
        }
    }

    private User[] getUsers() {
        return new User[]{
                new User(null, "3806734536743", "Adolf", "Hitlerl",
                        "German", "adolfyk_1945@gmail.com"),
                new User(null, "3809942434543", "Joseph", "Stalin",
                        "Soviet Union", "joseph_1941@gmail.com"),
                new User(null, "3809923153421", "Taras", "Bulba",
                        "Ukraine", "bulba_100500@gmail.com"),
        };
    }

}
