package com.ra.shop.controller;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.dto.UserDTO;
import com.ra.shop.model.User;
import com.ra.shop.repository.IRepository;
import com.ra.shop.web.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class UserControllerIntegrationTest {

    @Autowired
    private IRepository<User> dao;
    @Autowired
    private UserController orderController;

    private MockMvc mockMvc;
    private User testUser;
    private User testUserUpdate;
    private UserDTO userDTO;
    private long IDlastEntity;

    @BeforeEach
    void setUp() throws Exception {
        dao.getAll().stream().forEach(i -> {
            dao.delete(i.getId());
        });
        testUser = new User(null, "3809978957860", "Pasha", "Vakula",
                "Poland", "vakula_2123@gmail.com");
        testUserUpdate = new User(null, "3806378957877", "Gugulya", "Zahrema",
                "Turkey", "sdr_2123@gmail.com");
        userDTO = new UserDTO(null, testUser.getPhoneNumber(), testUser.getName(),
                testUser.getSecondName(), testUser.getCountry(), testUser.getEmailAddress());
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/user")
                .flashAttr("user", userDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("user", userDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        IDlastEntity = (dao.getAll().get(dao.getAll().size() - 1)).getId();
        testUser.setId(IDlastEntity);
        assertEquals(testUser, dao.get(testUser.getId()).get());
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/user")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("user",
                        dao.getAll().stream()
                                .map(entity -> new UserDTO
                                        (entity.getId(), entity.getPhoneNumber(), entity.getName(),
                                                entity.getSecondName(), entity.getCountry(), entity.getEmailAddress()))
                                .collect(Collectors.toList())))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showUser"));
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() {
        assertAll(() -> {
            assertEquals(testUser.getPhoneNumber(), dao.get(IDlastEntity).get().getPhoneNumber());
            assertEquals(testUser.getName(), dao.get(IDlastEntity).get().getName());
            assertEquals(testUser.getSecondName(), dao.get(IDlastEntity).get().getSecondName());
            assertEquals(testUser.getCountry(), dao.get(IDlastEntity).get().getCountry());
            assertEquals(testUser.getEmailAddress(), dao.get(IDlastEntity).get().getEmailAddress());
        });
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        testUserUpdate.setId(IDlastEntity);
        var userDTOupd = new UserDTO(IDlastEntity, testUserUpdate.getPhoneNumber(),
                testUserUpdate.getName(), testUserUpdate.getSecondName(),
                testUserUpdate.getCountry(), testUserUpdate.getEmailAddress());
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/user/{param.id}", IDlastEntity)
                .flashAttr("user", userDTOupd)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("user", userDTOupd))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        assertEquals(testUserUpdate, dao.get(testUserUpdate.getId()).get());
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/user/{param.id}", IDlastEntity)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/user"));
        assertTrue(dao.getAll().indexOf(testUser) == -1);
    }
}
