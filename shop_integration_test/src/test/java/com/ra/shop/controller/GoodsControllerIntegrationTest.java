package com.ra.shop.controller;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.dto.GoodsDTO;
import com.ra.shop.model.Goods;
import com.ra.shop.repository.IRepository;
import com.ra.shop.web.GoodsController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class GoodsControllerIntegrationTest {

    @Autowired
    private IRepository<Goods> dao;
    @Autowired
    private GoodsController goodsController;

    private MockMvc mockMvc;
    private Goods testGoods;
    private Goods testGoodsUpdate;
    private GoodsDTO goodsDTO;
    private long IDlastEntity;

    @BeforeEach
    void setUp() throws Exception {
        dao.getAll().stream().forEach(i -> {
            dao.delete(i.getId());
        });
        testGoods = new Goods(null, "Camel", 7622210609779l, 1.2d);
        testGoodsUpdate = new Goods(null, "Parlament", 7622210722416l, 2.6d);
        goodsDTO = new GoodsDTO(null, testGoods.getName(), testGoods.getBarcode(), testGoods.getPrice());
        mockMvc = MockMvcBuilders.standaloneSetup(goodsController).build();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/goods")
                .flashAttr("goods", goodsDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("goods", goodsDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        IDlastEntity = (dao.getAll().get(dao.getAll().size() - 1)).getId();
        testGoods.setId(IDlastEntity);
        assertEquals(testGoods, dao.get(testGoods.getId()).get());
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/goods")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("goods",
                        dao.getAll().stream()
                                .map(entity -> new GoodsDTO
                                        (entity.getId(), entity.getName(), entity.getBarcode(), entity.getPrice()))
                                .collect(Collectors.toList())))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showGoods"));
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() {
        assertAll(() -> {
            assertEquals(testGoods.getName(), dao.get(IDlastEntity).get().getName());
            assertEquals(testGoods.getBarcode(), dao.get(IDlastEntity).get().getBarcode());
            assertEquals(testGoods.getPrice(), dao.get(IDlastEntity).get().getPrice());
        });
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        testGoodsUpdate.setId(IDlastEntity);
        var goodsDTOupd = new GoodsDTO(IDlastEntity, testGoodsUpdate.getName(),
                testGoodsUpdate.getBarcode(), testGoodsUpdate.getPrice());
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/goods/{param.id}", IDlastEntity)
                .flashAttr("goods", goodsDTOupd)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("goods", goodsDTOupd))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        assertEquals(testGoodsUpdate, dao.get(testGoodsUpdate.getId()).get());
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/goods/{param.id}", IDlastEntity)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/goods"));
        assertTrue(dao.getAll().indexOf(testGoods) == -1);
    }
}
