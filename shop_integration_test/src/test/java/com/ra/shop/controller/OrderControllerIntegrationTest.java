package com.ra.shop.controller;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.dto.OrderDTO;
import com.ra.shop.model.Order;
import com.ra.shop.repository.IRepository;
import com.ra.shop.web.OrderController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class OrderControllerIntegrationTest {

    @Autowired
    private IRepository<Order> dao;
    @Autowired
    private OrderController orderController;

    private MockMvc mockMvc;
    private Order testOrder;
    private Order testOrderUpdate;
    private OrderDTO orderDTO;
    private long IDlastEntity;

    @BeforeEach
    void setUp() throws Exception {
        dao.getAll().stream().forEach(i -> {
            dao.delete(i.getId());
        });
        testOrder = new Order(null, 10, 90d, false,
                0, false);
        testOrderUpdate = new Order(null, 10, 900d, true,
                120, false);
        orderDTO = new OrderDTO(null, testOrder.getNumber(), testOrder.getPrice(),
                testOrder.getDeliveryIncluded(), testOrder.getDeliveryCost(), testOrder.getExecuted());
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/order")
                .flashAttr("order", orderDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("order", orderDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        IDlastEntity = (dao.getAll().get(dao.getAll().size() - 1)).getId();
        testOrder.setId(IDlastEntity);
        assertEquals(testOrder, dao.get(testOrder.getId()).get());
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/order")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("order",
                        dao.getAll().stream()
                                .map(entity -> new OrderDTO
                                        (entity.getId(), entity.getNumber(), entity.getPrice(),
                                                entity.getDeliveryIncluded(), entity.getDeliveryCost(), entity.getExecuted()))
                                .collect(Collectors.toList())))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showOrder"));
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() {
        assertAll(() -> {
            assertEquals(testOrder.getNumber(), dao.get(IDlastEntity).get().getNumber());
            assertEquals(testOrder.getPrice(), dao.get(IDlastEntity).get().getPrice());
            assertEquals(testOrder.getDeliveryCost(), dao.get(IDlastEntity).get().getDeliveryCost());
            assertEquals(testOrder.getDeliveryIncluded(), dao.get(IDlastEntity).get().getDeliveryIncluded());
            assertEquals(testOrder.getExecuted(), dao.get(IDlastEntity).get().getExecuted());
        });
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        testOrderUpdate.setId(IDlastEntity);
        var orderDTOupd = new OrderDTO(IDlastEntity, testOrderUpdate.getNumber(),
                testOrderUpdate.getPrice(), testOrderUpdate.getDeliveryIncluded(),
                testOrderUpdate.getDeliveryCost(), testOrderUpdate.getExecuted());
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/order/{param.id}", IDlastEntity)
                .flashAttr("order", orderDTOupd)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("order", orderDTOupd))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        assertEquals(testOrderUpdate, dao.get(testOrderUpdate.getId()).get());
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/order/{param.id}", IDlastEntity)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/order"));
        assertTrue(dao.getAll().indexOf(testOrder) == -1);
    }
}
