package com.ra.shop.controller;

import com.ra.shop.config.ShopConfiguration;
import com.ra.shop.dto.WarehouseDTO;
import com.ra.shop.model.Warehouse;
import com.ra.shop.repository.IRepository;
import com.ra.shop.web.WarehouseController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ShopConfiguration.class})
@WebAppConfiguration
public class WarehouseControllerIntegrationTest {

    @Autowired
    private IRepository<Warehouse> dao;
    @Autowired
    private WarehouseController warehouseController;

    private MockMvc mockMvc;
    private Warehouse testWarehouse;
    private Warehouse testWarehouseUpdate;
    private WarehouseDTO warehouseDTO;
    private long IDlastEntity;

    @BeforeEach
    void setUp() throws Exception {
        dao.getAll().stream().forEach(i -> {
            dao.delete(i.getId());
        });
        testWarehouse = new Warehouse(null, "Lol", 1.1, 2);
        testWarehouseUpdate = new Warehouse(null, "Test", 5.5, 9);
        warehouseDTO = new WarehouseDTO(null, testWarehouse.getName(), testWarehouse.getPrice(), testWarehouse.getAmount());
        mockMvc = MockMvcBuilders.standaloneSetup(warehouseController).build();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/warehouse")
                .flashAttr("warehouse", warehouseDTO)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse", warehouseDTO))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        IDlastEntity = (dao.getAll().get(dao.getAll().size() - 1)).getId();
        testWarehouse.setId(IDlastEntity);
        assertEquals(testWarehouse, dao.get(testWarehouse.getId()).get());
    }

    @Test
    void requestWithPathGetReturnsStatusOkAndCorrespondingUrlAndAttribute() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/warehouse")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse",
                        dao.getAll().stream()
                                .map(entity -> new WarehouseDTO
                                        (entity.getId(), entity.getName(), entity.getPrice(), entity.getAmount()))
                                .collect(Collectors.toList())))
                .andExpect(MockMvcResultMatchers.forwardedUrl("showWarehouse"));
    }

    @Test
    void requestWithPathPostReturnsRedirectionUrlandModel() {
        assertAll(() -> {
            assertEquals(testWarehouse.getName(), dao.get(IDlastEntity).get().getName());
            assertEquals(testWarehouse.getPrice(), dao.get(IDlastEntity).get().getPrice());
            assertEquals(testWarehouse.getAmount(), dao.get(IDlastEntity).get().getAmount());
        });
    }

    @Test
    void requestWithPathPutReturnsRedirectionUrlModelId() throws Exception {
        testWarehouseUpdate.setId(IDlastEntity);
        var warehouseDTOupd = new WarehouseDTO(IDlastEntity, testWarehouseUpdate.getName(),
                testWarehouseUpdate.getPrice(), testWarehouseUpdate.getAmount());
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/warehouse/{param.id}", IDlastEntity)
                .flashAttr("warehouse", warehouseDTOupd)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.model().attribute("warehouse", warehouseDTOupd))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        assertEquals(testWarehouseUpdate, dao.get(testWarehouseUpdate.getId()).get());
    }

    @Test
    void requestWithPathDeleteReturnsRedirectionUrlandlId() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/warehouse/{param.id}", IDlastEntity)
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .accept(MediaType.APPLICATION_XML)
                .characterEncoding("UTF-8");
        mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/warehouse"));
        assertTrue(dao.getAll().indexOf(testWarehouse) == -1);
    }
}
